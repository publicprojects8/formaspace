﻿using System;
using System.Collections.Generic;

namespace Formation.Models.DataBase
{
    public partial class SheetTypeOperation
    {
        public SheetTypeOperation()
        {
            SheetOperations = new HashSet<SheetOperation>();
        }

        public long SheetTypeOperationId { get; set; }
        public string Text { get; set; } = null!;

        public virtual ICollection<SheetOperation> SheetOperations { get; set; }
    }
}
