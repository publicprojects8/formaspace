﻿using System;
using System.Collections.Generic;

namespace Formation.Models.DataBase
{
    public partial class Equipment
    {
        public Equipment()
        {
            Sheets = new HashSet<Sheet>();
        }

        public long EquipmentId { get; set; }
        public string Name { get; set; } = null!;

        public virtual ICollection<Sheet> Sheets { get; set; }
    }
}
