﻿using System;
using System.Collections.Generic;

namespace Formation.Models.DataBase
{
    public partial class Level
    {
        public Level()
        {
            Certifications = new HashSet<Certification>();
            SheetOperations = new HashSet<SheetOperation>();
        }

        public long LevelId { get; set; }
        public long Level1 { get; set; }
        public string? LevelColor { get; set; }
        public string LevelName { get; set; } = null!;

        public virtual ICollection<Certification> Certifications { get; set; }
        public virtual ICollection<SheetOperation> SheetOperations { get; set; }
    }
}
