﻿using System;
using System.Collections.Generic;

namespace Formation.Models.DataBase
{
    public partial class Sheet
    {
        public Sheet()
        {
            Evaluations = new HashSet<Evaluation>();
            SheetOperations = new HashSet<SheetOperation>();
        }

        public long SheetId { get; set; }
        public long? EquipmentId { get; set; }

        public virtual Equipment? Equipment { get; set; }
        public virtual ICollection<Evaluation> Evaluations { get; set; }
        public virtual ICollection<SheetOperation> SheetOperations { get; set; }
    }
}
