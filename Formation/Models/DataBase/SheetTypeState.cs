﻿using System;
using System.Collections.Generic;

namespace Formation.Models.DataBase
{
    public partial class SheetTypeState
    {
        public SheetTypeState()
        {
            SheetOperations = new HashSet<SheetOperation>();
        }

        public long SheetStateOperationId { get; set; }
        public string Text { get; set; } = null!;

        public virtual ICollection<SheetOperation> SheetOperations { get; set; }
    }
}
