﻿using System;
using System.Collections.Generic;

namespace Formation.Models.DataBase
{
    public partial class Category
    {
        public Category()
        {
            Points = new HashSet<Point>();
        }

        public long CategoryId { get; set; }
        public string Name { get; set; } = null!;

        public virtual ICollection<Point> Points { get; set; }
    }
}
