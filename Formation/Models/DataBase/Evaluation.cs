﻿using System;
using System.Collections.Generic;

namespace Formation.Models.DataBase
{
    public partial class Evaluation
    {
        public Evaluation()
        {
            Points = new HashSet<Point>();
        }

        public long EvaluationId { get; set; }
        public DateTime DateStartEval { get; set; }
        public DateTime? DateEndEval { get; set; }
        public string? ObserverComment { get; set; }
        public long? EmployeeId { get; set; }
        public long? SheetId { get; set; }
        public long? EmployeeId1 { get; set; }

        public virtual Employee? Employee { get; set; }
        public virtual Employee? EmployeeId1Navigation { get; set; }
        public virtual Sheet? Sheet { get; set; }
        public virtual Certification? Certification { get; set; }

        public virtual ICollection<Point> Points { get; set; }
    }
}
