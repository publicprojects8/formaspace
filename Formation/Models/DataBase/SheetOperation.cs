﻿using System;
using System.Collections.Generic;

namespace Formation.Models.DataBase
{
    public partial class SheetOperation
    {
        public long SheetOperationId { get; set; }
        public DateTime DateOperation { get; set; }
        public DateTime? DateSaved { get; set; }
        public long? SheetId { get; set; }
        public long? LevelId { get; set; }
        public long? PointId { get; set; }
        public long? SheetTypeOperationId { get; set; }
        public long? SheetStateOperationId { get; set; }

        public virtual Level? Level { get; set; }
        public virtual Point? Point { get; set; }
        public virtual Sheet? Sheet { get; set; }
        public virtual SheetTypeState? SheetStateOperation { get; set; }
        public virtual SheetTypeOperation? SheetTypeOperation { get; set; }
    }
}
