﻿using System;
using System.Collections.Generic;

namespace Formation.Models.DataBase
{
    public partial class Certification
    {
        public long CertificationId { get; set; }
        public DateTime StartDate { get; set; }
        public DateTime EndDate { get; set; }
        public long? LevelId { get; set; }
        public long? EvaluationId { get; set; }

        public virtual Evaluation? Evaluation { get; set; }
        public virtual Level? Level { get; set; }
    }
}
