﻿using System;
using System.Collections.Generic;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata;

namespace Formation.Models.DataBase
{
    public partial class FormationContext : DbContext
    {
        public FormationContext()
        {
        }

        public FormationContext(DbContextOptions<FormationContext> options)
            : base(options)
        {
        }

        public virtual DbSet<Category> Categories { get; set; } = null!;
        public virtual DbSet<Certification> Certifications { get; set; } = null!;
        public virtual DbSet<Employee> Employees { get; set; } = null!;
        public virtual DbSet<Equipment> Equipment { get; set; } = null!;
        public virtual DbSet<Evaluation> Evaluations { get; set; } = null!;
        public virtual DbSet<Level> Levels { get; set; } = null!;
        public virtual DbSet<Point> Points { get; set; } = null!;
        public virtual DbSet<Sheet> Sheets { get; set; } = null!;
        public virtual DbSet<SheetOperation> SheetOperations { get; set; } = null!;
        public virtual DbSet<SheetTypeOperation> SheetTypeOperations { get; set; } = null!;
        public virtual DbSet<SheetTypeState> SheetTypeStates { get; set; } = null!;

        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            if (!optionsBuilder.IsConfigured)
            {
#warning To protect potentially sensitive information in your connection string, you should move it out of source code. You can avoid scaffolding the connection string by using the Name= syntax to read it from configuration - see https://go.microsoft.com/fwlink/?linkid=2131148. For more guidance on storing connection strings, see http://go.microsoft.com/fwlink/?LinkId=723263.
                optionsBuilder.UseSqlServer("Server=(localdb)\\MSSQLLocalDB;Database=Formation;Trusted_Connection=True;");
            }
        }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<Category>(entity =>
            {
                entity.ToTable("Category");

                entity.HasIndex(e => e.Name, "UQ__Category__737584F6741EE702")
                    .IsUnique();

                entity.Property(e => e.CategoryId).HasColumnName("CategoryID");

                entity.Property(e => e.Name)
                    .HasMaxLength(255)
                    .IsUnicode(false);
            });

            modelBuilder.Entity<Certification>(entity =>
            {
                entity.ToTable("Certification");

                entity.HasIndex(e => e.EvaluationId, "UQ__Certific__36AE68D27DC87D50")
                    .IsUnique();

                entity.Property(e => e.CertificationId).HasColumnName("CertificationID");

                entity.Property(e => e.EndDate).HasColumnType("datetime");

                entity.Property(e => e.EvaluationId).HasColumnName("EvaluationID");

                entity.Property(e => e.LevelId).HasColumnName("LevelID");

                entity.Property(e => e.StartDate).HasColumnType("datetime");

                entity.HasOne(d => d.Evaluation)
                    .WithOne(p => p.Certification)
                    .HasForeignKey<Certification>(d => d.EvaluationId)
                    .HasConstraintName("FK__Certifica__Evalu__5DB5E0CB");

                entity.HasOne(d => d.Level)
                    .WithMany(p => p.Certifications)
                    .HasForeignKey(d => d.LevelId)
                    .HasConstraintName("FK__Certifica__Level__5CC1BC92");
            });

            modelBuilder.Entity<Employee>(entity =>
            {
                entity.ToTable("Employee");

                entity.Property(e => e.EmployeeId).HasColumnName("EmployeeID");

                entity.Property(e => e.FirstName)
                    .HasMaxLength(255)
                    .IsUnicode(false);

                entity.Property(e => e.LastName)
                    .HasMaxLength(255)
                    .IsUnicode(false);
            });

            modelBuilder.Entity<Equipment>(entity =>
            {
                entity.HasIndex(e => e.Name, "UQ__Equipmen__737584F646CCD7D6")
                    .IsUnique();

                entity.Property(e => e.EquipmentId).HasColumnName("EquipmentID");

                entity.Property(e => e.Name)
                    .HasMaxLength(255)
                    .IsUnicode(false);
            });

            modelBuilder.Entity<Evaluation>(entity =>
            {
                entity.ToTable("Evaluation");

                entity.Property(e => e.EvaluationId).HasColumnName("EvaluationID");

                entity.Property(e => e.DateEndEval).HasColumnType("datetime");

                entity.Property(e => e.DateStartEval).HasColumnType("datetime");

                entity.Property(e => e.EmployeeId).HasColumnName("EmployeeID");

                entity.Property(e => e.EmployeeId1).HasColumnName("EmployeeID_1");

                entity.Property(e => e.ObserverComment)
                    .HasMaxLength(255)
                    .IsUnicode(false);

                entity.Property(e => e.SheetId).HasColumnName("SheetID");

                entity.HasOne(d => d.Employee)
                    .WithMany(p => p.EvaluationEmployees)
                    .HasForeignKey(d => d.EmployeeId)
                    .HasConstraintName("FK__Evaluatio__Emplo__4AA30C57");

                entity.HasOne(d => d.EmployeeId1Navigation)
                    .WithMany(p => p.EvaluationEmployeeId1Navigations)
                    .HasForeignKey(d => d.EmployeeId1)
                    .HasConstraintName("FK__Evaluatio__Emplo__4C8B54C9");

                entity.HasOne(d => d.Sheet)
                    .WithMany(p => p.Evaluations)
                    .HasForeignKey(d => d.SheetId)
                    .HasConstraintName("FK__Evaluatio__Sheet__4B973090");
            });

            modelBuilder.Entity<Level>(entity =>
            {
                entity.ToTable("Level");

                entity.HasIndex(e => e.LevelName, "UQ__Level__9EF3BE7B7C6C3139")
                    .IsUnique();

                entity.HasIndex(e => e.Level1, "UQ__Level__AAF89962855BE093")
                    .IsUnique();

                entity.Property(e => e.LevelId).HasColumnName("LevelID");

                entity.Property(e => e.Level1).HasColumnName("Level");

                entity.Property(e => e.LevelColor)
                    .HasMaxLength(255)
                    .IsUnicode(false);

                entity.Property(e => e.LevelName)
                    .HasMaxLength(255)
                    .IsUnicode(false);
            });

            modelBuilder.Entity<Point>(entity =>
            {
                entity.Property(e => e.PointId).HasColumnName("PointID");

                entity.Property(e => e.CategoryId).HasColumnName("CategoryID");

                entity.Property(e => e.Text)
                    .HasMaxLength(255)
                    .IsUnicode(false);

                entity.HasOne(d => d.Category)
                    .WithMany(p => p.Points)
                    .HasForeignKey(d => d.CategoryId)
                    .HasConstraintName("FK__Points__Category__58F12BAE");

                entity.HasMany(d => d.Evaluations)
                    .WithMany(p => p.Points)
                    .UsingEntity<Dictionary<string, object>>(
                        "EvPo",
                        l => l.HasOne<Evaluation>().WithMany().HasForeignKey("EvaluationId").OnDelete(DeleteBehavior.ClientSetNull).HasConstraintName("FK__Ev_Po__Evaluatio__68336F3E"),
                        r => r.HasOne<Point>().WithMany().HasForeignKey("PointId").OnDelete(DeleteBehavior.ClientSetNull).HasConstraintName("FK__Ev_Po__PointID__673F4B05"),
                        j =>
                        {
                            j.HasKey("PointId", "EvaluationId").HasName("PK__Ev_Po__63C3910C086EBC82");

                            j.ToTable("Ev_Po");

                            j.IndexerProperty<long>("PointId").HasColumnName("PointID");

                            j.IndexerProperty<long>("EvaluationId").HasColumnName("EvaluationID");
                        });
            });

            modelBuilder.Entity<Sheet>(entity =>
            {
                entity.Property(e => e.SheetId).HasColumnName("SheetID");

                entity.Property(e => e.EquipmentId).HasColumnName("EquipmentID");

                entity.HasOne(d => d.Equipment)
                    .WithMany(p => p.Sheets)
                    .HasForeignKey(d => d.EquipmentId)
                    .HasConstraintName("FK__Sheets__Equipmen__44EA3301");
            });

            modelBuilder.Entity<SheetOperation>(entity =>
            {
                entity.ToTable("SheetOperation");

                entity.Property(e => e.SheetOperationId).HasColumnName("SheetOperationID");

                entity.Property(e => e.DateOperation).HasColumnType("datetime");

                entity.Property(e => e.DateSaved).HasColumnType("datetime");

                entity.Property(e => e.LevelId).HasColumnName("LevelID");

                entity.Property(e => e.PointId).HasColumnName("PointID");

                entity.Property(e => e.SheetId).HasColumnName("SheetID");

                entity.Property(e => e.SheetStateOperationId).HasColumnName("SheetStateOperationID");

                entity.Property(e => e.SheetTypeOperationId).HasColumnName("SheetTypeOperationID");

                entity.HasOne(d => d.Level)
                    .WithMany(p => p.SheetOperations)
                    .HasForeignKey(d => d.LevelId)
                    .HasConstraintName("FK__SheetOper__Level__618671AF");

                entity.HasOne(d => d.Point)
                    .WithMany(p => p.SheetOperations)
                    .HasForeignKey(d => d.PointId)
                    .HasConstraintName("FK__SheetOper__Point__627A95E8");

                entity.HasOne(d => d.Sheet)
                    .WithMany(p => p.SheetOperations)
                    .HasForeignKey(d => d.SheetId)
                    .HasConstraintName("FK__SheetOper__Sheet__60924D76");

                entity.HasOne(d => d.SheetStateOperation)
                    .WithMany(p => p.SheetOperations)
                    .HasForeignKey(d => d.SheetStateOperationId)
                    .HasConstraintName("FK__SheetOper__Sheet__6462DE5A");

                entity.HasOne(d => d.SheetTypeOperation)
                    .WithMany(p => p.SheetOperations)
                    .HasForeignKey(d => d.SheetTypeOperationId)
                    .HasConstraintName("FK__SheetOper__Sheet__636EBA21");
            });

            modelBuilder.Entity<SheetTypeOperation>(entity =>
            {
                entity.ToTable("SheetTypeOperation");

                entity.HasIndex(e => e.Text, "UQ__SheetTyp__8F7960A34C209E65")
                    .IsUnique();

                entity.Property(e => e.SheetTypeOperationId).HasColumnName("SheetTypeOperationID");

                entity.Property(e => e.Text)
                    .HasMaxLength(255)
                    .IsUnicode(false);
            });

            modelBuilder.Entity<SheetTypeState>(entity =>
            {
                entity.HasKey(e => e.SheetStateOperationId)
                    .HasName("PK__SheetTyp__67FE55F36B5BFDAF");

                entity.ToTable("SheetTypeState");

                entity.HasIndex(e => e.Text, "UQ__SheetTyp__8F7960A34A69C50B")
                    .IsUnique();

                entity.Property(e => e.SheetStateOperationId).HasColumnName("SheetStateOperationID");

                entity.Property(e => e.Text)
                    .HasMaxLength(255)
                    .IsUnicode(false);
            });

            OnModelCreatingPartial(modelBuilder);
        }

        partial void OnModelCreatingPartial(ModelBuilder modelBuilder);
    }
}
