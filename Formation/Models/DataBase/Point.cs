﻿using System;
using System.Collections.Generic;

namespace Formation.Models.DataBase
{
    public partial class Point
    {
        public Point()
        {
            SheetOperations = new HashSet<SheetOperation>();
            Evaluations = new HashSet<Evaluation>();
        }

        public long PointId { get; set; }
        public string? Text { get; set; }
        public long? CategoryId { get; set; }

        public virtual Category? Category { get; set; }
        public virtual ICollection<SheetOperation> SheetOperations { get; set; }

        public virtual ICollection<Evaluation> Evaluations { get; set; }
    }
}
