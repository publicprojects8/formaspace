﻿using System;
using System.Collections.Generic;

namespace Formation.Models.DataBase
{
    public partial class Employee
    {
        public Employee()
        {
            EvaluationEmployeeId1Navigations = new HashSet<Evaluation>();
            EvaluationEmployees = new HashSet<Evaluation>();
        }

        public long EmployeeId { get; set; }
        public string FirstName { get; set; } = null!;
        public string LastName { get; set; } = null!;

        public virtual ICollection<Evaluation> EvaluationEmployeeId1Navigations { get; set; }
        public virtual ICollection<Evaluation> EvaluationEmployees { get; set; }
    }
}
