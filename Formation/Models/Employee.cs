﻿using System;
using System.Collections.Generic;

namespace Formation.Models.DataBase
{
    public partial class Employee
    {
        public string Identity
        {
            get
            {
                return FirstName + " " + LastName;
            }
        }
    }
}
