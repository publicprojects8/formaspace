﻿

var ModalPointSelector = function (api_category, api_point, modal, deleguator_when_point_selected) {
    this.api_category = api_category;
    this.api_point = api_point;
    this.deleguator_when_point_selected = deleguator_when_point_selected;
    this.modal = modal;
    this.current_value = { category: null, point: null };

    modalPointSelector = this;

    this.showGeneralError = function () {
        alert("Error");
    }

    this.showCategoryError = function () {
        console.log("Category IS REQUIRED ");
    }

    this.showPointError = function () {
        console.log("Point IS REQUIRED ");
    }

    this.select_category = function () {
        var name = $("#modpt_cat_in", modalPointSelector.modal).val();
        if (!name) {
            $("#modpt_pt_in", modalPointSelector.modal).prop("disabled", true);
            $("#modpt_pt_in", modalPointSelector.modal).val("");
            return;
        }
        var option = $('#modpt_cat_data option[value="' + name + '"]', modalPointSelector.modal);
        modalPointSelector.current_value.category = { categoryID: (option.length == 0 ? null : option.attr("data-value")), name: name };
        $("#modpt_pt_in", modalPointSelector.modal).prop("disabled", false);
    }

    this.select_point = function () {
        var text = $("#modpt_pt_in", modal).val();
        console.log("blabla " + text);
        if (!text) {
            return;
        }
        var option = $('#modpt_pt_data option[value="' + text + '"]', modalPointSelector.modal);
        modalPointSelector.current_value.point = { pointID: (option.length == 0 ? null : option.attr("data-value")), text: text };
    }

    this.validate = function () {
        console.log("blop");
        if (!modalPointSelector.current_value.category || !modalPointSelector.current_value.category.name) {
            modalPointSelector.showCategoryError();
            return;
        }
        if (!modalPointSelector.current_value.point || !modalPointSelector.current_value.point.text) {
            modalPointSelector.showPointError();
            return;
        }

        if (!modalPointSelector.current_value.category.categoryID) {
            var result = api_category.create(modalPointSelector.current_value.category.name);
            if (!result.categoryId) { modalPointSelector.showGeneralError(result); return;}
            modalPointSelector.current_value.category.categoryID = result.categoryId;
        }
        if (!modalPointSelector.current_value.point.pointID) {
            var result = api_point.create(modalPointSelector.current_value.point.text, modalPointSelector.current_value.category.categoryID);
            if (!result.pointId) { modalPointSelector.showGeneralError(result); return; }
            modalPointSelector.current_value.point.pointID = result.pointId;
        }

        deleguator_when_point_selected(modalPointSelector.current_value);
    }

    $("#btn_submit", modal).click(this.validate);
    $("#modpt_cat_in", modal).change(this.select_category);
    $("#modpt_pt_in", modal).change(this.select_point);
}
