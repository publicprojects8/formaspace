﻿


var SheetOperation = function (getAllURL, getOneURL, createURL, deleteURL, saveURL){
    this.getAllURL = getAllURL;
    this.getOneURL = getOneURL;
    this.createURL = createURL;
    this.deleteURL = deleteURL;
    this.saveURL = saveURL;
    console.log(createURL);

    this.getAll = function (sheetID) {
        console.log("Request - Get All Sheet Operations For Sheet ID :" + point_id);
        return $.ajax({
            type: "GET",
            async: false,
            url: getAllURL,
            data: {
                SheetID: sheetID
            },
            success: function (data) {
                console.log("Response - Get All Sheet Operations For Sheet ID :" + sheetID);
                return data;
            },
            error: function (error) {
                console.log("Response Error - Get All Sheet Operations For Sheet ID :" + sheetID + " - " + error);
                return error;
            }
        });
    };

    this.getOne = function (sheetOperationID) {
            console.log("Request - Get One Sheet Operations For Sheet Operation ID :" + sheetOperationID);
            return $.ajax({
                type: "GET",
                async: false,
                url: getOneURL,
                data: {
                    id: sheetOperationID
                },
                success: function (data) {
                    console.log("Response - Get All Sheet Operations For Sheet ID :" + sheetOperationID);
                    return data;
                },
                error: function (error) {
                    console.log("Response Error - Get All Sheet Operations For Sheet ID :" + sheetOperationID + " - " + error);
                    return error;
                }
            });
        };

    this.create = function (sheetID, pointID, levelID, type) {
        console.log("Request - Create Sheet Operation For Sheet ID :" + sheetID + ", PointID :" + pointID + ", Type :" + type + ", LevelID : " + levelID);
        console.log(createURL);
        var so = {};
        $.ajax({
            type: "POST",
            async: false,
            url: createURL,
            data: JSON.stringify({
                SheetID: sheetID,
                PointID: pointID,
                LevelId: levelID,
                SheetTypeOperation: { Text: type }
            }),
            contentType: "application/json",
            success: function (data) {
                console.log("Response - Create Sheet Operation For Sheet ID :" + sheetID + ", PointID :" + pointID + ", Type :" + type);
                so = data;
            },
            error: function (error) {
                console.log("Response Error - Create Sheet Operation For Sheet ID :" + sheetID + ", PointID :" + pointID + ", Type :" + type + " - " + error);
            }
        });
        return so;
    }
}