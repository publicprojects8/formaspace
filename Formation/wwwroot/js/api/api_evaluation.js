﻿


var Evaluation = function (getAllURL, getOneURL, createURL, saveURL, checkURL, uncheckURL, closeURL){
    this.getAllURL = getAllURL;
    this.getOneURL = getOneURL;
    this.createURL = createURL;
    this.checkURL = checkURL;
    this.uncheckURL = uncheckURL;
    this.saveURL = saveURL;
    this.closeURL = closeURL;
    evaluation = this;

    this.getAll = function () {
        console.log("Request - Get All Evaluations");
        return $.ajax({
            type: "GET",
            async: false,
            url: evaluation.getAllURL,
            data: { },
            success: function (data) {
                console.log("Response - Get All Evaluations");
                return data;
            },
            error: function (error) {
                console.log("Response Error - Get All Evaluations" + error);
                return error;
            }
        });
    };

    this.getOne = function (sheetID) {
        console.log("Request - Get One Evaluation For ID :" + sheetID);
        return $.ajax({
            type: "GET",
            async: false,
            url: evaluation.getOneURL,
            data: {
                id: sheetID
            },
            success: function (data) {
                console.log("Response - Get All Evaluations For ID :" + sheetID);
                return data;
            },
            error: function (error) {
                console.log("Response Error - Get All Evaluations For  ID :" + sheetID + " - " + error);
                return error;
            }
        });
    };


    this.check = function (evaluationID, pointID) {
        console.log("Request - Check Point For Evaluation ID :" + evaluationID);
        console.log(pointID);
        return $.ajax({
            type: "POST",
            async: false,
            url: evaluation.checkURL,
            contentType: "application/json",
            data: JSON.stringify({ EvaluationID: evaluationID, PointID: pointID}),
            success: function (data) {
                console.log("Response - Check Point For Evaluation ID :" + evaluationID);
                return data;
            },
            error: function (error) {
                console.log("Response Error - Check Point For Evaluation ID :" + evaluationID + " - " + error);
                console.log(error);
                return error;
            }
        });
    }

    this.uncheck = function (evaluationID, pointID) {
        console.log("Request - Uncheck Point For Evaluation ID :" + evaluationID);
        return $.ajax({
            type: "POST",
            async: false,
            url: evaluation.uncheckURL,
            contentType: "application/json",
            data: JSON.stringify({ EvaluationID: evaluationID, PointID: pointID }),
            success: function (data) {
                console.log("Response - Uncheck Point For Evaluation ID :" + evaluationID);
                return data;
            },
            error: function (error) {
                console.log("Response Error - Uncheck Point For Evaluation ID :" + evaluationID + " - " + error);
                return error;
            }
        });
    }

    this.close = function (evaluationID) {
        console.log("Request - Close Evaluation ID :" + evaluationID);
        return $.ajax({
            type: "POST",
            async: false,
            url: evaluation.closeURL + "/" + evaluationID,
            success: function (data) {
                console.log("Response - Uncheck Point For Evaluation ID :" + evaluationID);
                return data;
            },
            error: function (error) {
                console.log("Response Error - Uncheck Point For Evaluation ID :" + evaluationID + " - " + error);
                return error;
            }
        });
    }
}