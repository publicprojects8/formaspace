﻿
var Point = function (getAllURL, getOneURL, createURL, deleteURL) {
    this.getAllURL = getAllURL;
    this.getOneURL = getOneURL;
    this.createURL = createURL;
    this.deleteURL = deleteURL;

    this.getAll = function () {
        console.log("Request - Get All Points");
        return $.ajax({
            type: "GET",
            async: false,
            url: getAllURL,
            success: function (data) {
                console.log("Response - Get All Points");
                return data;
            },
            error: function (error) {
                console.log("Response Error - Get All Points" + " - " + error);
                return error;
            }
        });
    };

    this.getOne = function (pointID) {
        console.log("Request - Get One Point For Point ID :" + pointID);
        return $.ajax({
            type: "GET",
            async: false,
            url: getOneURL,
            data: {
                id: pointID
            },
            success: function (data) {
                console.log("Response - Get One Point For Point ID :" + pointID);
                return data;
            },
            error: function (error) {
                console.log("Response Error - Get One Point For Point ID :" + pointID + " - " + error);
                return error;
            }
        });
    };

    this.create = function (text, categoryID) {
        console.log("Request - Create Point For Text :" + text + ", CategoryID :" + categoryID);
        var point = {};
        $.ajax({
            type: "POST",
            async: false,
            url: createURL,
            data: JSON.stringify({
                Text: text,
                CategoryID: categoryID
            }),
            contentType: "application/json",
            success: function (data) {
                console.log("Response - Create Point For Text :" + text + ", CategoryID :" + categoryID);
                point = data;
            },
            error: function (error) {
                console.log("Response Error - Create Point For Text :" + text + ", CategoryID :" + categoryID + " - " + error);
            }
        });
        return point;
    }
}