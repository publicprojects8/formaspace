﻿


var Sheet = function (getAllURL, getOneURL, createURL, saveURL){
    this.getAllURL = getAllURL;
    this.getOneURL = getOneURL;
    this.createURL = createURL;
    this.saveURL = saveURL;
    sheet = this;

    this.getAll = function () {
        console.log("Request - Get All Sheets");
        return $.ajax({
            type: "GET",
            async: false,
            url: sheet.getAllURL,
            data: { },
            success: function (data) {
                console.log("Response - Get All Sheets");
                return data;
            },
            error: function (error) {
                console.log("Response Error - Get All Sheets" + error);
                return error;
            }
        });
    };

    this.getOne = function (sheetID) {
            console.log("Request - Get One Sheet For ID :" + sheetID);
            return $.ajax({
                type: "GET",
                async: false,
                url: sheet.getOneURL,
                data: {
                    id: sheetID
                },
                success: function (data) {
                    console.log("Response - Get All Sheet For ID :" + sheetID);
                    return data;
                },
                error: function (error) {
                    console.log("Response Error - Get All Sheet For  ID :" + sheetID + " - " + error);
                    return error;
                }
            });
        };


    this.save = function (sheetID) {
        console.log("Request - Save Sheet For Sheet ID :" + sheetID);
        return $.ajax({
            type: "POST",
            async: false,
            url: sheet.saveURL + "/" + sheetID,
            data: JSON.stringify({ }),
            success: function (data) {
                console.log("Response - Save Sheet For Sheet ID :" + sheetID);
                return data;
            },
            error: function (error) {
                console.log("Response Error - Save Sheet For Sheet ID :" + sheetID + " - " + error);
                return error;
            }
        });
    }
}