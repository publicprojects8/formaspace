﻿


var Category = function (getAllURL, getOneURL, createURL, deleteURL) {
    this.getAllURL = getAllURL;
    this.getOneURL = getOneURL;
    this.createURL = createURL;
    this.deleteURL = deleteURL;

    this.getAll = function() {
        console.log("Request - Get All Categories");
        return $.ajax({
            type: "GET",
            async: false,
            url: getAllURL,
            success: function (data) {
                console.log("Response - Get All Categories");
                return data;
            },
            error: function (error) {
                console.log("Response Error - Get All Categories" + " - " + error);
                return error;
            }
        });
    };

    this.getOne = function (categoryID) {
        console.log("Request - Get One Category For Category ID :" + categoryID);
        return $.ajax({
            type: "GET",
            async: false,
            url: getOneURL,
            data: {
                id: pointID
            },
            success: function (data) {
                console.log("Response - Get One Category For Category ID :" + categoryID);
                return data;
            },
            error: function (error) {
                console.log("Response Error - Get One Category For Category ID :" + categoryID + " - " + error);
                return error;
            }
        });
    };

    this.create = function (text) {
        console.log("Request - Create Category For Text :" + text);
        var category = {};
        $.ajax({
            type: "POST",
            async: false,
            url: createURL,
            data: JSON.stringify({
                Name: text
            }),
            contentType: "application/json",
            success: function (data) {
                console.log("Response - Create Category For Text :" + text);
                category = data
            },
            error: function (error) {
                console.log("Response Error - Create Category For Text :" + text + " - " + error);
            }
        });
        return category;
    }
}