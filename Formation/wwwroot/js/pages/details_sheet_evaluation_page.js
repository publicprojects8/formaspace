﻿



var DetailsSheetEvaluation = function (evaluationID, partialview_catURL, partialview_pointURL) {

    this.evaluationID = evaluationID;
    this.partialview_catURL = partialview_catURL;
    this.partialview_pointURL = partialview_pointURL;

    detailsSheetEvaluation = this;


    this.check = function (pointID) {
        detailsSheetEvaluation.api_evaluation.check(DetailsSheetEvaluation.evaluationID, pointID);
        $("#points_" + pointID).find("input:checkbox")[0].checked = true;
    }

    this.uncheck = function (pointID) {
        detailsSheetEvaluation.api_evaluation.uncheck(DetailsSheetEvaluation.evaluationID, pointID);
        $("#points_" + pointID).find("input:checkbox")[0].checked = false;
    }

    this.close = function () {
        detailsSheetEvaluation.api_evaluation.close(DetailsSheetEvaluation.evaluationID);
        window.location.href = view_detailsURL;
    }


    this.load_or_update_a_point = function (point_id) {
        console.log("Begin to Load/Update the point : " + point_id);
        $.ajax({
            type: "GET",
            url: partialview_pointURL,
            data: { PointID: point_id, EvaluationID: this.evaluationID },
            success: function (html_component) {
                console.log("End of Load/Update the point : " + point_id);
                $("#points_" + point_id).html(html_component);
            }
        });
    }

    this.load_or_update_a_category = function(category_id){
        console.log("Begin to Load/Update the category : " + category_id);
        $.ajax({
            type: "GET",
            url: partialview_catURL,
            data: { CategoryID: category_id, EvaluationID: this.evaluationID },
            success: function (html_component) {
                console.log("End of Load/Update the category : " + category_id);
                $("#category_" + category_id).html(html_component);
                $("#category_" + category_id + " table tbody").children().each(function (index, element) {
                    detailsSheetEvaluation.load_or_update_a_point($(element).attr("id").substring("points_".length))
                });
            }
        });
    }

    console.log("Initialisation");
    $("div[name='category']").each(function (index, element) {
        detailsSheetEvaluation.load_or_update_a_category($(element).attr("id").substring('category_'.length));
    });

}