﻿



var DetailsSheet = function (sheetID, partialview_catURL, partialview_pointURL) {

    this.sheetID = sheetID;
    this.partialview_catURL = partialview_catURL;
    this.partialview_pointURL = partialview_pointURL;

    editSheet = this;

    this.load_or_update_a_point = function (point_id) {
        console.log("Begin to Load/Update the point : " + point_id);
        $.ajax({
            type: "GET",
            url: partialview_pointURL,
            data: { pointID: point_id, SheetID: this.sheetID },
            success: function (html_component) {
                console.log("End of Load/Update the point : " + point_id);
                $("#points_" + point_id).html(html_component);
            }
        });
    }

    this.load_or_update_a_category = function(category_id){
        console.log("Begin to Load/Update the category : " + category_id);
        $.ajax({
            type: "GET",
            url: partialview_catURL,
            data: { categoryID: category_id, SheetID: this.sheetID },
            success: function (html_component) {
                console.log("End of Load/Update the category : " + category_id);
                if ($("#category_" + category_id).length == 0) {
                    $("#categories").append('<div name="category" id="category_' + category_id + '"></div>')
                }

                $("#category_" + category_id).html(html_component);
                $("#category_" + category_id + " table tbody").children().each(function (index, element) {
                    editSheet.load_or_update_a_point($(element).attr("id").substring("points_".length))
                });
            }
        });
    }

    console.log("Initialisation");
    $("div[name='category']").each(function (index, element) {
        editSheet.load_or_update_a_category($(element).attr("id").substring('category_'.length));
    });

}