﻿



var EditSheetEvaluation = function (evaluationID, view_detailsURL, partialview_catURL, partialview_pointURL, api_category, api_point, api_sheet, api_sheet_operation, api_evaluation) {

    this.evaluationID = evaluationID;
    this.view_detailsURL = view_detailsURL;
    this.api_category = api_category;
    this.api_point = api_point;
    this.api_sheet_operation = api_sheet_operation;
    this.api_sheet = api_sheet;
    this.api_evaluation = api_evaluation;
    this.partialview_catURL = partialview_catURL;
    this.partialview_pointURL = partialview_pointURL;

    editSheetEvaluation = this;


    this.check = function (pointID) {
        editSheetEvaluation.api_evaluation.check(editSheetEvaluation.evaluationID, pointID);
        $("#points_" + pointID).find("input:checkbox")[0].checked = true;
    }

    this.uncheck = function (pointID) {
        editSheetEvaluation.api_evaluation.uncheck(editSheetEvaluation.evaluationID, pointID);
        $("#points_" + pointID).find("input:checkbox")[0].checked = false;
    }

    this.close = function () {
        editSheetEvaluation.api_evaluation.close(editSheetEvaluation.evaluationID);
        window.location.href = view_detailsURL + "/" + evaluationID;
    }


    this.load_or_update_a_point = function (point_id) {
        console.log("Begin to Load/Update the point : " + point_id);
        $.ajax({
            type: "GET",
            url: partialview_pointURL,
            data: { PointID: point_id, EvaluationID: this.evaluationID },
            success: function (html_component) {
                console.log("End of Load/Update the point : " + point_id);
                $("#points_" + point_id).html(html_component);
                $("#points_" + point_id).find("input:checkbox").change(function () {
                    if ($(this)[0].checked) {
                        editSheetEvaluation.check(point_id);
                    } else {
                        editSheetEvaluation.uncheck(point_id);
                    }
                });
            }
        });
    }

    this.load_or_update_a_category = function(category_id){
        console.log("Begin to Load/Update the category : " + category_id);
        $.ajax({
            type: "GET",
            url: partialview_catURL,
            data: { CategoryID: category_id, EvaluationID: this.evaluationID },
            success: function (html_component) {
                console.log("End of Load/Update the category : " + category_id);
                $("#category_" + category_id).html(html_component);
                $("#category_" + category_id + " table tbody").children().each(function (index, element) {
                    editSheetEvaluation.load_or_update_a_point($(element).attr("id").substring("points_".length))
                });
            }
        });
    }

    console.log("Initialisation");
    $("div[name='category']").each(function (index, element) {
        editSheetEvaluation.load_or_update_a_category($(element).attr("id").substring('category_'.length));
    });

    $("#btn_close").click(editSheetEvaluation.close);

}