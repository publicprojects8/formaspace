﻿



var EditSheet = function (sheetID, partialview_catURL, partialview_pointURL, api_category, api_point, api_sheet, api_sheet_operation) {

    this.sheetID = sheetID;
    this.api_category = api_category;
    this.api_point = api_point;
    this.api_sheet_operation = api_sheet_operation;
    this.api_sheet = api_sheet;
    this.partialview_catURL = partialview_catURL;
    this.partialview_pointURL = partialview_pointURL;

    editSheet = this;

    this.add_Point = function (point) {
        editSheet.create_sheetoperation(point, null, "Add");
        editSheet.load_or_update_a_category();
    }

    this.modify_Point = function (pointID, levelID) {
        editSheet.create_sheetoperation(pointID, levelID, "Modify");
        editSheet.load_or_update_a_point(pointID);
    }

    this.remove_Point = function (pointID) {
        editSheet.create_sheetoperation(pointID, null, "Remove");
        editSheet.load_or_update_a_point(pointID);
    }

    this.create_sheetoperation = function (pointID, levelID, type) {
        api_sheet_operation.create(this.sheetID, pointID, levelID, type);
    }

    this.remove_selected_points = function() {
        $('input:checkbox[name="check_point"]').each(function (index, element) {
            if (!$(element)[0].checked) return;
            var pointID = $(element).val().substring("point_".length);
            editSheet.remove_Point(pointID);
        });
    }

    this.load_or_update_a_point = function (point_id) {
        console.log("Begin to Load/Update the point : " + point_id);
        $.ajax({
            type: "GET",
            url: partialview_pointURL,
            data: { pointID: point_id, SheetID: this.sheetID },
            success: function (html_component) {
                console.log("End of Load/Update the point : " + point_id);
                $("#points_" + point_id).html(html_component);
                $("#points_" + point_id).find("select[name='LevelID']").change(function () {
                    editSheet.modify_Point(point_id, $(this).find(":selected").val())
                });
            }
        });
    }

    this.load_or_update_a_category = function(category_id){
        console.log("Begin to Load/Update the category : " + category_id);
        $.ajax({
            type: "GET",
            url: partialview_catURL,
            data: { categoryID: category_id, SheetID: this.sheetID },
            success: function (html_component) {
                console.log("End of Load/Update the category : " + category_id);
                if ($("#category_" + category_id).length == 0) {
                    $("#categories").append('<div name="category" id="category_' + category_id + '"></div>')
                }

                $("#category_" + category_id).html(html_component);
                $("#category_" + category_id + " table tbody").children().each(function (index, element) {
                    editSheet.load_or_update_a_point($(element).attr("id").substring("points_".length))
                });
                $("#category_" + category_id + " table thead tr th input:checkbox").change(function () {
                    state = $(this)[0].checked;
                    $("#category_" + category_id + " table tbody tr td input:checkbox").each(function () {
                        $(this)[0].checked = state;
                    });
                });
            }
        });
    }

    this.point_chosen = function (point) {
        editSheet.api_sheet_operation.create(editSheet.sheetID, point.point.pointID, null, "Add");

        editSheet.load_or_update_a_category(point.category.categoryID);
    }

    this.save = function () {
        api_sheet.save(editSheet.sheetID);
        $(".pt-rmv, .pt-add").each(function (index, element) {
            var trparent = $(element).parent();
            editSheet.load_or_update_a_point(trparent.attr("id").substring('points_'.length));
        });
        alert("Saved");
    }


    console.log("Initialisation");
    $("div[name='category']").each(function (index, element) {
        editSheet.load_or_update_a_category($(element).attr("id").substring('category_'.length));
    });
    $("#btn_removable").click(editSheet.remove_selected_points);

    $("#btn_save").click(editSheet.save);

}