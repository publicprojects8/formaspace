﻿



var DetailsEmployee = function (employeeID, partialview_evaluationURL, partialview_certificationURL) {

    this.employeeID = employeeID;
    this.partialview_evaluationURL = partialview_evaluationURL;
    this.partialview_certificationURL = partialview_certificationURL;

    detailsEmployee = this;

    this.load_or_update_evaluations = function () {
        console.log("Begin to Load/Update the evaluations");
        $.ajax({
            type: "GET",
            url: partialview_evaluationURL,
            data: { id: employeeID },
            success: function (html_component) {
                console.log("End of Load/Update the evaluations");
                $("#evaluations").html(html_component);
                $("#evaluations").find("table").DataTable({
                    /*language: {
                        "url": "//cdn.datatables.net/plug-ins/9dcbecd42ad/i18n/French.json"
                    },*/
                    "autoWidth": false,
                    "scrollCollapse": true,
                    "paging": true,
                    "pageLength": 10,
                    "stateSave": true
                });
            }
        });
    }

    this.load_or_update_certifications = function () {
        console.log("Begin to Load/Update the evaluations");
        $.ajax({
            type: "GET",
            url: partialview_certificationURL,
            data: { id: employeeID },
            success: function (html_component) {
                console.log("End of Load/Update the evaluations");
                $("#certifications").html(html_component);
                $("#certifications").find("table").DataTable({
                    /*language: {
                        "url": "//cdn.datatables.net/plug-ins/9dcbecd42ad/i18n/French.json"
                    },*/
                    "autoWidth": false,
                    "scrollCollapse": true,
                    "paging": true,
                    "pageLength": 10,
                    "stateSave": true
                });
            }
        });
    }

    console.log("Initialisation");
    detailsEmployee.load_or_update_evaluations();
    detailsEmployee.load_or_update_certifications();
}