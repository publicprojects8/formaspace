﻿using Formation.Models;
using Formation.Models.DataBase;
using Microsoft.AspNetCore.Mvc;
using System.Diagnostics;

namespace Formation.Controllers
{
    public class HomeController : Controller
    {
        private readonly ILogger<HomeController> _logger;
        private readonly FormationContext _context;

        public HomeController(ILogger<HomeController> logger, FormationContext context)
        {
            _logger = logger;
            _context = context;
        }

        public IActionResult Index()
        {
            return View();
        }

        public IActionResult Privacy()
        {
            return View();
        }

        [ResponseCache(Duration = 0, Location = ResponseCacheLocation.None, NoStore = true)]
        public IActionResult Error()
        {
            return View(new ErrorViewModel { RequestId = Activity.Current?.Id ?? HttpContext.TraceIdentifier });
        }



        public async void Generate()
        {
            _context.SheetOperations.RemoveRange(_context.SheetOperations);
            _context.SaveChanges();
            _context.Certifications.RemoveRange(_context.Certifications);
            _context.SaveChanges();
            _context.Evaluations.RemoveRange(_context.Evaluations);
            _context.SaveChanges();
            _context.Sheets.RemoveRange(_context.Sheets);
            _context.SaveChanges();
            _context.Points.RemoveRange(_context.Points);
            _context.SaveChanges();
            _context.Categories.RemoveRange(_context.Categories);
            _context.SaveChanges();
            _context.Equipment.RemoveRange(_context.Equipment);
            _context.SaveChanges();
            _context.Employees.RemoveRange(_context.Employees);
            _context.SaveChanges();
            _context.Levels.RemoveRange(_context.Levels);
            _context.SaveChanges();
            _context.SheetTypeOperations.RemoveRange(_context.SheetTypeOperations);
            _context.SaveChanges();
            _context.SheetTypeStates.RemoveRange(_context.SheetTypeStates);
            _context.SaveChanges();


            var Employees = new List<Employee>
            {
                new Employee{FirstName = "Michel", LastName = "Courtemanche"},
                new Employee{FirstName = "Gustave", LastName = "Ruais"},
                new Employee{FirstName = "Alexandre", LastName = "Lacroix"},
                new Employee{FirstName = "Gano", LastName = "Beauchemin"},
                new Employee{FirstName = "Turner", LastName = "Vernadeau"},

                new Employee{FirstName = "Henry", LastName = "Paradis"},
                new Employee{FirstName = "Brigliador", LastName = "Devost"},
                new Employee{FirstName = "Russell", LastName = "Bélanger"},
                new Employee{FirstName = "Marlon", LastName = "Lamontagne"},
                new Employee{FirstName = "Harbin", LastName = "De La Vergne"},

                new Employee{FirstName = "Martha", LastName = "Machado"},
                new Employee{FirstName = "Pryor", LastName = "Lécuyer"},
                new Employee{FirstName = "Denis", LastName = "Larocque"},
                new Employee{FirstName = "Bartlett", LastName = "Rancourt"},
                new Employee{FirstName = "Madelene", LastName = "Parent"},

                new Employee{FirstName = "Kerman", LastName = "Dodier"},
                new Employee{FirstName = "Clementine", LastName = "Guibord"},
                new Employee{FirstName = "Théophile", LastName = "Faure"},
                new Employee{FirstName = "Catherine", LastName = "René"},
                new Employee{FirstName = "Sylvie", LastName = "Lang"},
            };
            _context.Employees.AddRange(Employees);
            _context.SaveChanges();

            var Equipments = new List<Equipment>
            {
                new Equipment{ Name = "Station Soudage PA10" },
                new Equipment{ Name = "Station Sertissage B25" },
                new Equipment{ Name = "Station Assemblage P15 - M4" },
                new Equipment{ Name = "Station Assemblage P17 - P70" },
                new Equipment{ Name = "Station Assemblage P5 - R7" },

                new Equipment{ Name = "Poste PA10X" },
                new Equipment{ Name = "Poste PA11X" },
                new Equipment{ Name = "Poste PA12X" },
                new Equipment{ Name = "Poste PA13X" },
                new Equipment{ Name = "Poste PA14X" },

                new Equipment{ Name = "Poste B20" },
                new Equipment{ Name = "Poste B21" },
                new Equipment{ Name = "Poste B22" },
                new Equipment{ Name = "Poste B23" },
                new Equipment{ Name = "Poste B24" },

                new Equipment{ Name = "Station Assemblage 0001" },
                new Equipment{ Name = "Station Assemblage 0002" },
                new Equipment{ Name = "Station Assemblage 0003" },
                new Equipment{ Name = "Station Assemblage 0004" },
                new Equipment{ Name = "Station Assemblage 0005" },
            };
            _context.Equipment.AddRange(Equipments);
            _context.SaveChanges();

            var Sheets = new List<Sheet>();
            foreach(var eq in Equipments)
            {
                Sheets.Add(new Sheet { Equipment = eq });
            }
            _context.Sheets.AddRange(Sheets);
            _context.SaveChanges();

            var Categories = new List<Category>
            {
                new Category{ Name = "SECURITE / ENVIRONNEMENT" },
                new Category{ Name = "DOCUMENTATION" },
                new Category{ Name = "PRODUCTION" }
            };
            _context.Categories.AddRange(Categories);
            _context.SaveChanges();

            var Points = new List<Point>
            {
                new Point{ Text = "Connaissance XXXX", Category = Categories.ElementAt(0)},
                new Point{ Text = "Bon positionnement de la machine au repos", Category = Categories.ElementAt(0)},
                new Point{ Text = "Rangement des outils après utilisation", Category = Categories.ElementAt(0)},
                new Point{ Text = "Connaitre et savoir appliquer la procédure d''urgence", Category = Categories.ElementAt(0)},
                new Point{ Text = "Manipulation Correct des produits chimiques XXXX et XXXX", Category = Categories.ElementAt(0)},

                new Point{ Text = "Connaissance des outils", Category = Categories.ElementAt(1)},
                new Point{ Text = "Connaissance des procédures applicables au poste de travail", Category = Categories.ElementAt(1)},
                new Point{ Text = "Connaissance des caractéstiques spécifiques au produits XXXX et XXXX", Category = Categories.ElementAt(1)},

                new Point{ Text = "Imprimer et coller les étiquettes", Category = Categories.ElementAt(2)},
                new Point{ Text = "Installer la puce XXXX", Category = Categories.ElementAt(2)},
                new Point{ Text = "Désouder et retirer la puce XXXX", Category = Categories.ElementAt(2)}
            };
            _context.Points.AddRange(Points);
            _context.SaveChanges();


            var Add = new SheetTypeOperation { Text = "Add" };
            var Modify = new SheetTypeOperation { Text = "Modify" };
            var Remove = new SheetTypeOperation { Text = "Remove" };

            _context.SheetTypeOperations.Add(Add); _context.SheetTypeOperations.Add(Remove);
            _context.SheetTypeOperations.Add(Modify);

            _context.SaveChanges();

            var Saved = new SheetTypeState { Text = "Saved" };
            var Unsaved = new SheetTypeState { Text = "Unsaved" };

            _context.SheetTypeStates.Add(Saved); _context.SheetTypeStates.Add(Unsaved);

            _context.SaveChanges();

            var Levels = new List<Level>
            {
                new Level{ LevelName = "Débutant", LevelColor = HexConverter(System.Drawing.Color.Orange), Level1 = 1 },
                new Level{ LevelName = "Intermédiaire", LevelColor = HexConverter(System.Drawing.Color.Yellow), Level1 = 2 },
                new Level{ LevelName = "Expert", LevelColor = HexConverter(System.Drawing.Color.Green), Level1 = 3 },
                new Level{ LevelName = "Formateur", LevelColor = HexConverter(System.Drawing.Color.Blue),  Level1 = 4 },
            };
            _context.Levels.AddRange(Levels);
            _context.SaveChanges();

            var SheetOperations = new List<SheetOperation>
            {
                new SheetOperation{ Point = Points.ElementAt(0), DateSaved = DateTime.Now.AddDays(-1).AddHours(5), Sheet = Sheets.ElementAt(0), SheetTypeOperation = Add, SheetStateOperation = Saved, DateOperation = DateTime.Now.AddDays(-1), Level = Levels.ElementAt(0) },
                new SheetOperation{ Point = Points.ElementAt(1), DateSaved = DateTime.Now.AddDays(-1).AddHours(5), Sheet = Sheets.ElementAt(0), SheetTypeOperation = Add, SheetStateOperation = Saved, DateOperation = DateTime.Now.AddDays(-1), Level = Levels.ElementAt(0) },
                new SheetOperation{ Point = Points.ElementAt(2), DateSaved = DateTime.Now.AddDays(-1).AddHours(5), Sheet = Sheets.ElementAt(0), SheetTypeOperation = Add, SheetStateOperation = Saved, DateOperation = DateTime.Now.AddDays(-1), Level = Levels.ElementAt(1) },
                new SheetOperation{ Point = Points.ElementAt(3), DateSaved = DateTime.Now.AddDays(-1).AddHours(5), Sheet = Sheets.ElementAt(0), SheetTypeOperation = Add, SheetStateOperation = Saved, DateOperation = DateTime.Now.AddDays(-1), Level = Levels.ElementAt(2) },
                new SheetOperation{ Point = Points.ElementAt(4), DateSaved = DateTime.Now.AddDays(-1).AddHours(5), Sheet = Sheets.ElementAt(0), SheetTypeOperation = Add, SheetStateOperation = Saved, DateOperation = DateTime.Now.AddDays(-1), Level = Levels.ElementAt(2) },

                new SheetOperation{ Point = Points.ElementAt(5), DateSaved = DateTime.Now.AddDays(-1).AddHours(5), Sheet = Sheets.ElementAt(0), SheetTypeOperation = Add, SheetStateOperation = Saved, DateOperation = DateTime.Now.AddDays(-1), Level = Levels.ElementAt(2) },
                new SheetOperation{ Point = Points.ElementAt(6), DateSaved = DateTime.Now.AddDays(-1).AddHours(5), Sheet = Sheets.ElementAt(0), SheetTypeOperation = Add, SheetStateOperation = Saved, DateOperation = DateTime.Now.AddDays(-1), Level = Levels.ElementAt(2) },
                new SheetOperation{ Point = Points.ElementAt(7), DateSaved = DateTime.Now.AddDays(-1).AddHours(5), Sheet = Sheets.ElementAt(0), SheetTypeOperation = Add, SheetStateOperation = Saved, DateOperation = DateTime.Now.AddDays(-1), Level = Levels.ElementAt(3) },

                new SheetOperation{ Point = Points.ElementAt(8), DateSaved = DateTime.Now.AddDays(-1).AddHours(5), Sheet = Sheets.ElementAt(0), SheetTypeOperation = Add, SheetStateOperation = Saved, DateOperation = DateTime.Now.AddDays(-1), Level = Levels.ElementAt(2) },
                new SheetOperation{ Point = Points.ElementAt(9), DateSaved = DateTime.Now.AddDays(-1).AddHours(5), Sheet = Sheets.ElementAt(0), SheetTypeOperation = Add, SheetStateOperation = Saved, DateOperation = DateTime.Now.AddDays(-1), Level = Levels.ElementAt(0) },
                new SheetOperation{ Point = Points.ElementAt(10), DateSaved = DateTime.Now.AddDays(-1).AddHours(5), Sheet = Sheets.ElementAt(0), SheetTypeOperation = Add, SheetStateOperation = Saved, DateOperation = DateTime.Now.AddDays(-1), Level = Levels.ElementAt(3) },
                //new SheetOperation{ Point = Points.ElementAt(10), Sheet = Sheets.ElementAt(0), SheetTypeOperation = Remove, SheetStateOperation = Saved, DateOperation = DateTime.Now},
            };
            _context.SheetOperations.AddRange(SheetOperations);
            _context.SaveChanges();

            var Evaluations = new List<Evaluation>
            {
                new Evaluation{ Employee = Employees.ElementAt(0), DateStartEval = DateTime.Now.AddMinutes(2), DateEndEval= DateTime.Now.AddMinutes(3), Sheet = Sheets.ElementAt(0), ObserverComment = " Manque un peu d'experiences pour acquerir le niveau Expert", Certification = new Certification{ StartDate = DateTime.Now.AddMinutes(4), EndDate = DateTime.Now.AddMinutes(4).AddMonths(6), Level = Levels.ElementAt(1) } },
                new Evaluation{ Employee = Employees.ElementAt(0), DateStartEval = DateTime.Now.AddMinutes(2), DateEndEval= DateTime.Now.AddMinutes(3), Sheet = Sheets.ElementAt(1), ObserverComment = " ", Certification = new Certification{ StartDate = DateTime.Now.AddMinutes(4), EndDate = DateTime.Now.AddMinutes(4).AddMonths(6), Level = Levels.ElementAt(2) } },
                new Evaluation{ Employee = Employees.ElementAt(0), DateStartEval = DateTime.Now.AddMinutes(2), DateEndEval= DateTime.Now.AddMinutes(3), Sheet = Sheets.ElementAt(2), ObserverComment = " ", Certification = new Certification{ StartDate = DateTime.Now.AddMinutes(4), EndDate = DateTime.Now.AddMinutes(4).AddMonths(6), Level = Levels.ElementAt(3) } },
                new Evaluation{ Employee = Employees.ElementAt(0), DateStartEval = DateTime.Now.AddMinutes(2), DateEndEval= DateTime.Now.AddMinutes(3), Sheet = Sheets.ElementAt(3), ObserverComment = " ", Certification = new Certification{ StartDate = DateTime.Now.AddMinutes(4), EndDate = DateTime.Now.AddMinutes(4).AddMonths(6), Level = Levels.ElementAt(1) } },
                new Evaluation{ Employee = Employees.ElementAt(0), DateStartEval = DateTime.Now.AddMinutes(2), DateEndEval= DateTime.Now.AddMinutes(3), Sheet = Sheets.ElementAt(4), ObserverComment = " ", Certification = new Certification{ StartDate = DateTime.Now.AddMinutes(4), EndDate = DateTime.Now.AddMinutes(4).AddMonths(6), Level = Levels.ElementAt(3) } },

                new Evaluation{ Employee = Employees.ElementAt(0), DateStartEval = DateTime.Now.AddMinutes(2), DateEndEval= DateTime.Now.AddMinutes(3), Sheet = Sheets.ElementAt(4), ObserverComment = " ", Certification = new Certification{ StartDate = DateTime.Now.AddMinutes(4), EndDate = DateTime.Now.AddMinutes(4).AddMonths(6), Level = Levels.ElementAt(1) } },
                new Evaluation{ Employee = Employees.ElementAt(0), DateStartEval = DateTime.Now.AddMinutes(2), DateEndEval= DateTime.Now.AddMinutes(3), Sheet = Sheets.ElementAt(5), ObserverComment = " ", Certification = new Certification{ StartDate = DateTime.Now.AddMinutes(4), EndDate = DateTime.Now.AddMinutes(4).AddMonths(6), Level = Levels.ElementAt(3) } },
                new Evaluation{ Employee = Employees.ElementAt(1), DateStartEval = DateTime.Now.AddMinutes(2), DateEndEval= DateTime.Now.AddMinutes(3), Sheet = Sheets.ElementAt(6), ObserverComment = " ", Certification = new Certification{ StartDate = DateTime.Now.AddMinutes(4), EndDate = DateTime.Now.AddMinutes(4).AddMonths(6), Level = Levels.ElementAt(2) } },
                new Evaluation{ Employee = Employees.ElementAt(1), DateStartEval = DateTime.Now.AddMinutes(2), DateEndEval= DateTime.Now.AddMinutes(3), Sheet = Sheets.ElementAt(7), ObserverComment = " ", Certification = new Certification{ StartDate = DateTime.Now.AddMinutes(4), EndDate = DateTime.Now.AddMinutes(4).AddMonths(6), Level = Levels.ElementAt(0) } },
                new Evaluation{ Employee = Employees.ElementAt(1), DateStartEval = DateTime.Now.AddMinutes(2), DateEndEval= DateTime.Now.AddMinutes(3), Sheet = Sheets.ElementAt(6), ObserverComment = " ", Certification = new Certification{ StartDate = DateTime.Now.AddMinutes(4), EndDate = DateTime.Now.AddMinutes(4).AddMonths(6), Level = Levels.ElementAt(0) } },

                new Evaluation{ Employee = Employees.ElementAt(2), DateStartEval = DateTime.Now.AddMinutes(2), DateEndEval= DateTime.Now.AddMinutes(3), Sheet = Sheets.ElementAt(7), ObserverComment = " ", Certification = new Certification{ StartDate = DateTime.Now.AddMinutes(4), EndDate = DateTime.Now.AddMinutes(4).AddMonths(6), Level = Levels.ElementAt(3) } },
                new Evaluation{ Employee = Employees.ElementAt(2), DateStartEval = DateTime.Now.AddMinutes(2), DateEndEval= DateTime.Now.AddMinutes(3), Sheet = Sheets.ElementAt(8), ObserverComment = " ", Certification = new Certification{ StartDate = DateTime.Now.AddMinutes(4), EndDate = DateTime.Now.AddMinutes(4).AddMonths(6), Level = Levels.ElementAt(1) } },
                new Evaluation{ Employee = Employees.ElementAt(2), DateStartEval = DateTime.Now.AddMinutes(2), DateEndEval= DateTime.Now.AddMinutes(3), Sheet = Sheets.ElementAt(8), ObserverComment = " ", Certification = new Certification{ StartDate = DateTime.Now.AddMinutes(4), EndDate = DateTime.Now.AddMinutes(4).AddMonths(6), Level = Levels.ElementAt(2) } },
                new Evaluation{ Employee = Employees.ElementAt(2), DateStartEval = DateTime.Now.AddMinutes(2), DateEndEval= DateTime.Now.AddMinutes(3), Sheet = Sheets.ElementAt(8), ObserverComment = " ", Certification = new Certification{ StartDate = DateTime.Now.AddMinutes(4), EndDate = DateTime.Now.AddMinutes(4).AddMonths(6), Level = Levels.ElementAt(2) } },
                new Evaluation{ Employee = Employees.ElementAt(2), DateStartEval = DateTime.Now.AddMinutes(2), DateEndEval= DateTime.Now.AddMinutes(3), Sheet = Sheets.ElementAt(1), ObserverComment = " ", Certification = new Certification{ StartDate = DateTime.Now.AddMinutes(4), EndDate = DateTime.Now.AddMinutes(4).AddMonths(6), Level = Levels.ElementAt(3) } },

                new Evaluation{ Employee = Employees.ElementAt(3), DateStartEval = DateTime.Now.AddMinutes(2), DateEndEval= DateTime.Now.AddMinutes(3), Sheet = Sheets.ElementAt(9), ObserverComment = " ", Certification = new Certification{ StartDate = DateTime.Now.AddMinutes(4), EndDate = DateTime.Now.AddMinutes(4).AddMonths(6), Level = Levels.ElementAt(2) } },
                new Evaluation{ Employee = Employees.ElementAt(3), DateStartEval = DateTime.Now.AddMinutes(2), DateEndEval= DateTime.Now.AddMinutes(3), Sheet = Sheets.ElementAt(8), ObserverComment = " ", Certification = new Certification{ StartDate = DateTime.Now.AddMinutes(4), EndDate = DateTime.Now.AddMinutes(4).AddMonths(6), Level = Levels.ElementAt(0) } },
                new Evaluation{ Employee = Employees.ElementAt(3), DateStartEval = DateTime.Now.AddMinutes(2), DateEndEval= DateTime.Now.AddMinutes(3), Sheet = Sheets.ElementAt(4), ObserverComment = " ", Certification = new Certification{ StartDate = DateTime.Now.AddMinutes(4), EndDate = DateTime.Now.AddMinutes(4).AddMonths(6), Level = Levels.ElementAt(1) } },
                new Evaluation{ Employee = Employees.ElementAt(3), DateStartEval = DateTime.Now.AddMinutes(2), DateEndEval= DateTime.Now.AddMinutes(3), Sheet = Sheets.ElementAt(10), ObserverComment = " ", Certification = new Certification{ StartDate = DateTime.Now.AddMinutes(4), EndDate = DateTime.Now.AddMinutes(4).AddMonths(6), Level = Levels.ElementAt(3) } },
                new Evaluation{ Employee = Employees.ElementAt(3), DateStartEval = DateTime.Now.AddMinutes(2), DateEndEval= DateTime.Now.AddMinutes(3), Sheet = Sheets.ElementAt(11), ObserverComment = " ", Certification = new Certification{ StartDate = DateTime.Now.AddMinutes(4), EndDate = DateTime.Now.AddMinutes(4).AddMonths(6), Level = Levels.ElementAt(0) } },

                new Evaluation{ Employee = Employees.ElementAt(3), DateStartEval = DateTime.Now.AddMinutes(2), DateEndEval= DateTime.Now.AddMinutes(3), Sheet = Sheets.ElementAt(12), ObserverComment = " ", Certification = new Certification{ StartDate = DateTime.Now.AddMinutes(4), EndDate = DateTime.Now.AddMinutes(4).AddMonths(6), Level = Levels.ElementAt(3) } },
                new Evaluation{ Employee = Employees.ElementAt(3), DateStartEval = DateTime.Now.AddMinutes(2), DateEndEval= DateTime.Now.AddMinutes(3), Sheet = Sheets.ElementAt(13), ObserverComment = " ", Certification = new Certification{ StartDate = DateTime.Now.AddMinutes(4), EndDate = DateTime.Now.AddMinutes(4).AddMonths(6), Level = Levels.ElementAt(2) } },
                new Evaluation{ Employee = Employees.ElementAt(3), DateStartEval = DateTime.Now.AddMinutes(2), DateEndEval= DateTime.Now.AddMinutes(3), Sheet = Sheets.ElementAt(17), ObserverComment = " ", Certification = new Certification{ StartDate = DateTime.Now.AddMinutes(4), EndDate = DateTime.Now.AddMinutes(4).AddMonths(6), Level = Levels.ElementAt(3) } },
                new Evaluation{ Employee = Employees.ElementAt(4), DateStartEval = DateTime.Now.AddMinutes(2), DateEndEval= DateTime.Now.AddMinutes(3), Sheet = Sheets.ElementAt(7), ObserverComment = " ", Certification = new Certification{ StartDate = DateTime.Now.AddMinutes(4), EndDate = DateTime.Now.AddMinutes(4).AddMonths(6), Level = Levels.ElementAt(1) } },
                new Evaluation{ Employee = Employees.ElementAt(4), DateStartEval = DateTime.Now.AddMinutes(2), DateEndEval= DateTime.Now.AddMinutes(3), Sheet = Sheets.ElementAt(0), ObserverComment = " ", Certification = new Certification{ StartDate = DateTime.Now.AddMinutes(4), EndDate = DateTime.Now.AddMinutes(4).AddMonths(6), Level = Levels.ElementAt(3) } },

                new Evaluation{ Employee = Employees.ElementAt(4), DateStartEval = DateTime.Now.AddMinutes(2), DateEndEval= DateTime.Now.AddMinutes(3), Sheet = Sheets.ElementAt(14), ObserverComment = " ", Certification = new Certification{ StartDate = DateTime.Now.AddMinutes(4), EndDate = DateTime.Now.AddMinutes(4).AddMonths(6), Level = Levels.ElementAt(2) } },
                new Evaluation{ Employee = Employees.ElementAt(4), DateStartEval = DateTime.Now.AddMinutes(2), DateEndEval= DateTime.Now.AddMinutes(3), Sheet = Sheets.ElementAt(15), ObserverComment = " ", Certification = new Certification{ StartDate = DateTime.Now.AddMinutes(4), EndDate = DateTime.Now.AddMinutes(4).AddMonths(6), Level = Levels.ElementAt(0) } },
                new Evaluation{ Employee = Employees.ElementAt(5), DateStartEval = DateTime.Now.AddMinutes(2), DateEndEval= DateTime.Now.AddMinutes(3), Sheet = Sheets.ElementAt(17), ObserverComment = " ", Certification = new Certification{ StartDate = DateTime.Now.AddMinutes(4), EndDate = DateTime.Now.AddMinutes(4).AddMonths(6), Level = Levels.ElementAt(3) } },
                new Evaluation{ Employee = Employees.ElementAt(5), DateStartEval = DateTime.Now.AddMinutes(2), DateEndEval= DateTime.Now.AddMinutes(3), Sheet = Sheets.ElementAt(10), ObserverComment = " ", Certification = new Certification{ StartDate = DateTime.Now.AddMinutes(4), EndDate = DateTime.Now.AddMinutes(4).AddMonths(6), Level = Levels.ElementAt(3) } },
                new Evaluation{ Employee = Employees.ElementAt(5), DateStartEval = DateTime.Now.AddMinutes(2), DateEndEval= DateTime.Now.AddMinutes(3), Sheet = Sheets.ElementAt(7), ObserverComment = " ", Certification = new Certification{ StartDate = DateTime.Now.AddMinutes(4), EndDate = DateTime.Now.AddMinutes(4).AddMonths(6), Level = Levels.ElementAt(3) } },

                new Evaluation{ Employee = Employees.ElementAt(5), DateStartEval = DateTime.Now.AddMinutes(2), DateEndEval= DateTime.Now.AddMinutes(3), Sheet = Sheets.ElementAt(11), ObserverComment = " ", Certification = new Certification{ StartDate = DateTime.Now.AddMinutes(4), EndDate = DateTime.Now.AddMinutes(4).AddMonths(6), Level = Levels.ElementAt(2) } },
                new Evaluation{ Employee = Employees.ElementAt(6), DateStartEval = DateTime.Now.AddMinutes(2), DateEndEval= DateTime.Now.AddMinutes(3), Sheet = Sheets.ElementAt(12), ObserverComment = " ", Certification = new Certification{ StartDate = DateTime.Now.AddMinutes(4), EndDate = DateTime.Now.AddMinutes(4).AddMonths(6), Level = Levels.ElementAt(0) } },
                new Evaluation{ Employee = Employees.ElementAt(6), DateStartEval = DateTime.Now.AddMinutes(2), DateEndEval= DateTime.Now.AddMinutes(3), Sheet = Sheets.ElementAt(12), ObserverComment = " ", Certification = new Certification{ StartDate = DateTime.Now.AddMinutes(4), EndDate = DateTime.Now.AddMinutes(4).AddMonths(6), Level = Levels.ElementAt(0) } },
                new Evaluation{ Employee = Employees.ElementAt(6), DateStartEval = DateTime.Now.AddMinutes(2), DateEndEval= DateTime.Now.AddMinutes(3), Sheet = Sheets.ElementAt(17), ObserverComment = " ", Certification = new Certification{ StartDate = DateTime.Now.AddMinutes(4), EndDate = DateTime.Now.AddMinutes(4).AddMonths(6), Level = Levels.ElementAt(0) } },
                new Evaluation{ Employee = Employees.ElementAt(6), DateStartEval = DateTime.Now.AddMinutes(2), DateEndEval= DateTime.Now.AddMinutes(3), Sheet = Sheets.ElementAt(0), ObserverComment = " ", Certification = new Certification{ StartDate = DateTime.Now.AddMinutes(4), EndDate = DateTime.Now.AddMinutes(4).AddMonths(6), Level = Levels.ElementAt(0) } },

                new Evaluation{ Employee = Employees.ElementAt(7), DateStartEval = DateTime.Now.AddMinutes(2), DateEndEval= DateTime.Now.AddMinutes(3), Sheet = Sheets.ElementAt(12), ObserverComment = " ", Certification = new Certification{ StartDate = DateTime.Now.AddMinutes(4), EndDate = DateTime.Now.AddMinutes(4).AddMonths(6), Level = Levels.ElementAt(3) } },
                new Evaluation{ Employee = Employees.ElementAt(8), DateStartEval = DateTime.Now.AddMinutes(2), DateEndEval= DateTime.Now.AddMinutes(3), Sheet = Sheets.ElementAt(17), ObserverComment = " ", Certification = new Certification{ StartDate = DateTime.Now.AddMinutes(4), EndDate = DateTime.Now.AddMinutes(4).AddMonths(6), Level = Levels.ElementAt(2) } },
                new Evaluation{ Employee = Employees.ElementAt(8), DateStartEval = DateTime.Now.AddMinutes(2), DateEndEval= DateTime.Now.AddMinutes(3), Sheet = Sheets.ElementAt(7), ObserverComment = " ", Certification = new Certification{ StartDate = DateTime.Now.AddMinutes(4), EndDate = DateTime.Now.AddMinutes(4).AddMonths(6), Level = Levels.ElementAt(1) } },
                new Evaluation{ Employee = Employees.ElementAt(9), DateStartEval = DateTime.Now.AddMinutes(2), DateEndEval= DateTime.Now.AddMinutes(3), Sheet = Sheets.ElementAt(2), ObserverComment = " ", Certification = new Certification{ StartDate = DateTime.Now.AddMinutes(4), EndDate = DateTime.Now.AddMinutes(4).AddMonths(6), Level = Levels.ElementAt(3) } },
                new Evaluation{ Employee = Employees.ElementAt(10), DateStartEval = DateTime.Now.AddMinutes(2), DateEndEval= DateTime.Now.AddMinutes(3), Sheet = Sheets.ElementAt(3), ObserverComment = " ", Certification = new Certification{ StartDate = DateTime.Now.AddMinutes(4), EndDate = DateTime.Now.AddMinutes(4).AddMonths(6), Level = Levels.ElementAt(2) } },

                new Evaluation{ Employee = Employees.ElementAt(7), DateStartEval = DateTime.Now.AddMinutes(2), DateEndEval= DateTime.Now.AddMinutes(3), Sheet = Sheets.ElementAt(0), ObserverComment = " ", Certification = new Certification{ StartDate = DateTime.Now.AddMinutes(4), EndDate = DateTime.Now.AddMinutes(4).AddMonths(6), Level = Levels.ElementAt(2) } },
                new Evaluation{ Employee = Employees.ElementAt(8), DateStartEval = DateTime.Now.AddMinutes(2), DateEndEval= DateTime.Now.AddMinutes(3), Sheet = Sheets.ElementAt(1), ObserverComment = " ", Certification = new Certification{ StartDate = DateTime.Now.AddMinutes(4), EndDate = DateTime.Now.AddMinutes(4).AddMonths(6), Level = Levels.ElementAt(2) } },
                new Evaluation{ Employee = Employees.ElementAt(8), DateStartEval = DateTime.Now.AddMinutes(2), DateEndEval= DateTime.Now.AddMinutes(3), Sheet = Sheets.ElementAt(3), ObserverComment = " ", Certification = new Certification{ StartDate = DateTime.Now.AddMinutes(4), EndDate = DateTime.Now.AddMinutes(4).AddMonths(6), Level = Levels.ElementAt(2) } },
                new Evaluation{ Employee = Employees.ElementAt(9), DateStartEval = DateTime.Now.AddMinutes(2), DateEndEval= DateTime.Now.AddMinutes(3), Sheet = Sheets.ElementAt(0), ObserverComment = " ", Certification = new Certification{ StartDate = DateTime.Now.AddMinutes(4), EndDate = DateTime.Now.AddMinutes(4).AddMonths(6), Level = Levels.ElementAt(2) } },
                new Evaluation{ Employee = Employees.ElementAt(10), DateStartEval = DateTime.Now.AddMinutes(2), DateEndEval= DateTime.Now.AddMinutes(3), Sheet = Sheets.ElementAt(5), ObserverComment = " ", Certification = new Certification{ StartDate = DateTime.Now.AddMinutes(4), EndDate = DateTime.Now.AddMinutes(4).AddMonths(6), Level = Levels.ElementAt(2) } },

                new Evaluation{ Employee = Employees.ElementAt(7), DateStartEval = DateTime.Now.AddMinutes(2), DateEndEval= DateTime.Now.AddMinutes(3), Sheet = Sheets.ElementAt(0), ObserverComment = " ", Certification = new Certification{ StartDate = DateTime.Now.AddMinutes(4), EndDate = DateTime.Now.AddMinutes(4).AddMonths(6), Level = Levels.ElementAt(2) } },
                new Evaluation{ Employee = Employees.ElementAt(8), DateStartEval = DateTime.Now.AddMinutes(2), DateEndEval= DateTime.Now.AddMinutes(3), Sheet = Sheets.ElementAt(8), ObserverComment = " ", Certification = new Certification{ StartDate = DateTime.Now.AddMinutes(4), EndDate = DateTime.Now.AddMinutes(4).AddMonths(6), Level = Levels.ElementAt(3) } },
                new Evaluation{ Employee = Employees.ElementAt(8), DateStartEval = DateTime.Now.AddMinutes(2), DateEndEval= DateTime.Now.AddMinutes(3), Sheet = Sheets.ElementAt(0), ObserverComment = " ", Certification = new Certification{ StartDate = DateTime.Now.AddMinutes(4), EndDate = DateTime.Now.AddMinutes(4).AddMonths(6), Level = Levels.ElementAt(2) } },
                new Evaluation{ Employee = Employees.ElementAt(9), DateStartEval = DateTime.Now.AddMinutes(2), DateEndEval= DateTime.Now.AddMinutes(3), Sheet = Sheets.ElementAt(9), ObserverComment = " ", Certification = new Certification{ StartDate = DateTime.Now.AddMinutes(4), EndDate = DateTime.Now.AddMinutes(4).AddMonths(6), Level = Levels.ElementAt(0) } },
                new Evaluation{ Employee = Employees.ElementAt(10), DateStartEval = DateTime.Now.AddMinutes(2), DateEndEval= DateTime.Now.AddMinutes(3), Sheet = Sheets.ElementAt(0), ObserverComment = " ", Certification = new Certification{ StartDate = DateTime.Now.AddMinutes(4), EndDate = DateTime.Now.AddMinutes(4).AddMonths(6), Level = Levels.ElementAt(1) } },

                new Evaluation{ Employee = Employees.ElementAt(10), DateStartEval = DateTime.Now.AddMinutes(2), DateEndEval= DateTime.Now.AddMinutes(3), Sheet = Sheets.ElementAt(4), ObserverComment = " ", Certification = new Certification{ StartDate = DateTime.Now.AddMinutes(4), EndDate = DateTime.Now.AddMinutes(4).AddMonths(6), Level = Levels.ElementAt(1) } },
                new Evaluation{ Employee = Employees.ElementAt(10), DateStartEval = DateTime.Now.AddMinutes(2), DateEndEval= DateTime.Now.AddMinutes(3), Sheet = Sheets.ElementAt(3), ObserverComment = " ", Certification = new Certification{ StartDate = DateTime.Now.AddMinutes(4), EndDate = DateTime.Now.AddMinutes(4).AddMonths(6), Level = Levels.ElementAt(1) } },
                new Evaluation{ Employee = Employees.ElementAt(11), DateStartEval = DateTime.Now.AddMinutes(2), DateEndEval= DateTime.Now.AddMinutes(3), Sheet = Sheets.ElementAt(7), ObserverComment = " ", Certification = new Certification{ StartDate = DateTime.Now.AddMinutes(4), EndDate = DateTime.Now.AddMinutes(4).AddMonths(6), Level = Levels.ElementAt(1) } },
                new Evaluation{ Employee = Employees.ElementAt(11), DateStartEval = DateTime.Now.AddMinutes(2), DateEndEval= DateTime.Now.AddMinutes(3), Sheet = Sheets.ElementAt(16), ObserverComment = " ", Certification = new Certification{ StartDate = DateTime.Now.AddMinutes(4), EndDate = DateTime.Now.AddMinutes(4).AddMonths(6), Level = Levels.ElementAt(2) } },
                new Evaluation{ Employee = Employees.ElementAt(11), DateStartEval = DateTime.Now.AddMinutes(2), DateEndEval= DateTime.Now.AddMinutes(3), Sheet = Sheets.ElementAt(0), ObserverComment = " ", Certification = new Certification{ StartDate = DateTime.Now.AddMinutes(4), EndDate = DateTime.Now.AddMinutes(4).AddMonths(6), Level = Levels.ElementAt(2) } },

                new Evaluation{ Employee = Employees.ElementAt(11), DateStartEval = DateTime.Now.AddMinutes(2), DateEndEval= DateTime.Now.AddMinutes(3), Sheet = Sheets.ElementAt(14), ObserverComment = " ", Certification = new Certification{ StartDate = DateTime.Now.AddMinutes(4), EndDate = DateTime.Now.AddMinutes(4).AddMonths(6), Level = Levels.ElementAt(0) } },
                new Evaluation{ Employee = Employees.ElementAt(12), DateStartEval = DateTime.Now.AddMinutes(2), DateEndEval= DateTime.Now.AddMinutes(3), Sheet = Sheets.ElementAt(16), ObserverComment = " ", Certification = new Certification{ StartDate = DateTime.Now.AddMinutes(4), EndDate = DateTime.Now.AddMinutes(4).AddMonths(6), Level = Levels.ElementAt(0) } },
                new Evaluation{ Employee = Employees.ElementAt(13), DateStartEval = DateTime.Now.AddMinutes(2), DateEndEval= DateTime.Now.AddMinutes(3), Sheet = Sheets.ElementAt(13), ObserverComment = " ", Certification = new Certification{ StartDate = DateTime.Now.AddMinutes(4), EndDate = DateTime.Now.AddMinutes(4).AddMonths(6), Level = Levels.ElementAt(0) } },
                new Evaluation{ Employee = Employees.ElementAt(13), DateStartEval = DateTime.Now.AddMinutes(2), DateEndEval= DateTime.Now.AddMinutes(3), Sheet = Sheets.ElementAt(0), ObserverComment = " ", Certification = new Certification{ StartDate = DateTime.Now.AddMinutes(4), EndDate = DateTime.Now.AddMinutes(4).AddMonths(6), Level = Levels.ElementAt(1) } },
                new Evaluation{ Employee = Employees.ElementAt(14), DateStartEval = DateTime.Now.AddMinutes(2), DateEndEval= DateTime.Now.AddMinutes(3), Sheet = Sheets.ElementAt(10), ObserverComment = " ", Certification = new Certification{ StartDate = DateTime.Now.AddMinutes(4), EndDate = DateTime.Now.AddMinutes(4).AddMonths(6), Level = Levels.ElementAt(1) } },

                new Evaluation{ Employee = Employees.ElementAt(15), DateStartEval = DateTime.Now.AddMinutes(2), DateEndEval= DateTime.Now.AddMinutes(3), Sheet = Sheets.ElementAt(7), ObserverComment = " ", Certification = new Certification{ StartDate = DateTime.Now.AddMinutes(4), EndDate = DateTime.Now.AddMinutes(4).AddMonths(6), Level = Levels.ElementAt(2) } },
                new Evaluation{ Employee = Employees.ElementAt(15), DateStartEval = DateTime.Now.AddMinutes(2), DateEndEval= DateTime.Now.AddMinutes(3), Sheet = Sheets.ElementAt(19), ObserverComment = " ", Certification = new Certification{ StartDate = DateTime.Now.AddMinutes(4), EndDate = DateTime.Now.AddMinutes(4).AddMonths(6), Level = Levels.ElementAt(3) } },
                new Evaluation{ Employee = Employees.ElementAt(15), DateStartEval = DateTime.Now.AddMinutes(2), DateEndEval= DateTime.Now.AddMinutes(3), Sheet = Sheets.ElementAt(18), ObserverComment = " ", Certification = new Certification{ StartDate = DateTime.Now.AddMinutes(4), EndDate = DateTime.Now.AddMinutes(4).AddMonths(6), Level = Levels.ElementAt(3) } },
                new Evaluation{ Employee = Employees.ElementAt(16), DateStartEval = DateTime.Now.AddMinutes(2), DateEndEval= DateTime.Now.AddMinutes(3), Sheet = Sheets.ElementAt(18), ObserverComment = " ", Certification = new Certification{ StartDate = DateTime.Now.AddMinutes(4), EndDate = DateTime.Now.AddMinutes(4).AddMonths(6), Level = Levels.ElementAt(1) } },
                new Evaluation{ Employee = Employees.ElementAt(17), DateStartEval = DateTime.Now.AddMinutes(2), DateEndEval= DateTime.Now.AddMinutes(3), Sheet = Sheets.ElementAt(19), ObserverComment = " ", Certification = new Certification{ StartDate = DateTime.Now.AddMinutes(4), EndDate = DateTime.Now.AddMinutes(4).AddMonths(6), Level = Levels.ElementAt(0) } },

                new Evaluation{ Employee = Employees.ElementAt(18), DateStartEval = DateTime.Now.AddMinutes(2), DateEndEval= DateTime.Now.AddMinutes(3), Sheet = Sheets.ElementAt(4), ObserverComment = " ", Certification = new Certification{ StartDate = DateTime.Now.AddMinutes(4), EndDate = DateTime.Now.AddMinutes(4).AddMonths(6), Level = Levels.ElementAt(0) } },
                new Evaluation{ Employee = Employees.ElementAt(18), DateStartEval = DateTime.Now.AddMinutes(2), DateEndEval= DateTime.Now.AddMinutes(3), Sheet = Sheets.ElementAt(7), ObserverComment = " ", Certification = new Certification{ StartDate = DateTime.Now.AddMinutes(4), EndDate = DateTime.Now.AddMinutes(4).AddMonths(6), Level = Levels.ElementAt(1) } },
                new Evaluation{ Employee = Employees.ElementAt(19), DateStartEval = DateTime.Now.AddMinutes(2), DateEndEval= DateTime.Now.AddMinutes(3), Sheet = Sheets.ElementAt(18), ObserverComment = " ", Certification = new Certification{ StartDate = DateTime.Now.AddMinutes(4), EndDate = DateTime.Now.AddMinutes(4).AddMonths(6), Level = Levels.ElementAt(0) } },
                new Evaluation{ Employee = Employees.ElementAt(19), DateStartEval = DateTime.Now.AddMinutes(2), DateEndEval= DateTime.Now.AddMinutes(3), Sheet = Sheets.ElementAt(17), ObserverComment = " ", Certification = new Certification{ StartDate = DateTime.Now.AddMinutes(4), EndDate = DateTime.Now.AddMinutes(4).AddMonths(6), Level = Levels.ElementAt(1) } },
                new Evaluation{ Employee = Employees.ElementAt(19), DateStartEval = DateTime.Now.AddMinutes(2), DateEndEval= DateTime.Now.AddMinutes(3), Sheet = Sheets.ElementAt(19), ObserverComment = " ", Certification = new Certification{ StartDate = DateTime.Now.AddMinutes(4), EndDate = DateTime.Now.AddMinutes(4).AddMonths(6), Level = Levels.ElementAt(0) } },
            };
            _context.Evaluations.AddRange(Evaluations);
            _context.SaveChanges();

        }

        private string HexConverter(System.Drawing.Color color)
        {
            return "#" + color.R.ToString("X2") + color.G.ToString("X2") + color.B.ToString("X2");
        }


    }
}