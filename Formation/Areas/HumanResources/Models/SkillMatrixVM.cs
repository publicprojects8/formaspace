﻿using Formation.Models.DataBase;

namespace Formation.Areas.HumanResources.Models
{
    public class SkillMatrixVM
    {

        public IEnumerable<Employee> Employees { get; set; }
        public IEnumerable<Equipment> Equipments { get; set; }
        public IEnumerable<Certification> Certifications { get; set; }

    }
}
