﻿#nullable disable
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using Formation.Models.DataBase;
using Formation.Areas.HumanResources.Models;

namespace Formation.Areas.HumanResources.Controllers
{
    [Area("HumanResources")]
    public class CertificationsController : Controller
    {
        private readonly FormationContext _context;

        public CertificationsController(FormationContext context)
        {
            _context = context;
        }

        public async Task<IActionResult> SkillMatrix()
        {
            SkillMatrixVM outputViewModel = new SkillMatrixVM
            {
                Employees = await _context.Employees.ToListAsync(),
                Equipments = await _context.Equipment.ToListAsync(),
                Certifications = await _context.Certifications
                    .Include(c => c.Evaluation).ThenInclude(e => e.Sheet)
                    .Include(c => c.Level)
                    .Where(ce => ce.StartDate == _context.Certifications.Where(cem => cem.Evaluation.EmployeeId == ce.Evaluation.EmployeeId && cem.Evaluation.SheetId == ce.Evaluation.SheetId).Max(cem => cem.StartDate))
                    .ToListAsync()
            };
            return View(outputViewModel);
        }

        private bool CertificationExists(long id)
        {
            return _context.Certifications.Any(e => e.CertificationId == id);
        }
    }
}
