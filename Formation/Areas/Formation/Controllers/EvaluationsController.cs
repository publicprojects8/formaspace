﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using Formation.Models.DataBase;
using Formation.Areas.Formation.Models;

namespace Formation.Areas.Formation.Controllers
{
    [Area("Formation")]
    public class EvaluationsController : Controller
    {
        private readonly FormationContext _context;

        public EvaluationsController(FormationContext context)
        {
            _context = context;
        }

        public async Task<IActionResult> Create(long? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            Employee employee = await _context.Employees.FindAsync(id);

            if(employee == null) return NotFound();
            Evaluation evaluation = new Evaluation { Employee = employee, EmployeeId = employee.EmployeeId, DateStartEval=DateTime.Now };

            ViewData["Sheet"] = new SelectList(_context.Sheets.Include(sh => sh.Equipment), "SheetId", "Equipment.Name");
            ViewData["EmployeeEval"] = new SelectList(_context.Employees, "EmployeeId", "Identity");

            return View(evaluation);
        }

        // POST: Formation/Evaluations/Create
        // To protect from overposting attacks, enable the specific properties you want to bind to.
        // For more details, see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Create([Bind("EvaluationId,DateStartEval,ObserverComment,EmployeeId,SheetId,EmployeeId1")] Evaluation evaluation)
        {
            if (ModelState.IsValid)
            {
                _context.Add(evaluation);
                await _context.SaveChangesAsync();
                return RedirectToAction(nameof(Edit), new {id =evaluation.EvaluationId});
            }

            var employee = await _context.Employees.FindAsync(evaluation.EmployeeId);
            var sheet = await _context.Sheets.Include(sh => sh.Equipment).FirstOrDefaultAsync(eq => eq.SheetId == evaluation.SheetId);
            
            if (employee == null || sheet == null) return NotFound();

            ViewData["Employee"] = employee;
            ViewData["Workspace"] = sheet.Equipment;

            ViewData["EmployeeEval"] = new SelectList(_context.Employees, "EmployeeId", "Identity");
            return View(evaluation);
        }

        // GET: Formation/Evaluations/Edit/5
        public async Task<IActionResult> Edit(long? id)
        {
            if (id == null || _context.Evaluations == null)
            {
                return NotFound();
            }

            var evaluation = await _context
                .Evaluations
                .Include(ev => ev.Employee)
                .Include(ev => ev.Sheet)
                .ThenInclude(sh => sh.Equipment)
                .FirstOrDefaultAsync(ev => ev.EvaluationId == id);

            if (evaluation == null)
            {
                return NotFound();
            }

            ViewData["Categories"] = await _context.SheetOperations
                .Where(so =>
                   so.SheetId == evaluation.SheetId &&
                   so.SheetStateOperation.Text == "Saved" &&
                   so.SheetTypeOperation.Text != "Remove" &&
                   so.DateSaved >= _context.SheetOperations.Where(sod => sod.SheetId == so.SheetId && sod.PointId == so.PointId).Max(sod => sod.DateSaved)
                )
                .Select(so => so.Point.Category)
                .Distinct()
                .ToListAsync();

            return View("/Areas/Formation/Views/Evaluations/Edit/Edit.cshtml", evaluation);
        }

        public async Task<IActionResult> Edit_Table_Category([Bind("CategoryID, EvaluationID")] SheetEvaluationCategoryVM input)
        {
            Category category = await _context.Categories.FindAsync(input.CategoryID);
            Evaluation evaluation = await _context.Evaluations.FindAsync(input.EvaluationID);

            if (category == null || evaluation == null)
            {
                return NotFound();
            }

            ViewData["Points"] = await _context.Points
                .Where(p => p.CategoryId == category.CategoryId && p.SheetOperations.Any(so =>
                        so.SheetId == evaluation.SheetId
                        && so.DateSaved.HasValue
                        && so.SheetTypeOperation.Text != "Remove"
                        && so.DateSaved == _context.SheetOperations
                            .Where(sod => sod.SheetId == so.SheetId
                                && so.PointId == sod.PointId)
                            .Max(sod => sod.DateSaved))
                ).ToListAsync();

            return PartialView("/Areas/Formation/Views/Evaluations/Edit/Edit_Table_Category.cshtml", category);
        }

        public async Task<IActionResult> Edit_Table_Point_SO([Bind("PointID, EvaluationID")] SheetEvaluationPointVM input)
        {
            SheetOperation so = await _context.SheetOperations
                .Where(so => so.Sheet.Evaluations.Any(ev => ev.EvaluationId == input.EvaluationID) && so.PointId == input.PointID)
                .Include(so => so.SheetTypeOperation)
                .Include(so => so.Level)
                .Include(so => so.SheetStateOperation)
                .Include(so => so.Point)
                .OrderByDescending(so => so.DateOperation)
                .FirstOrDefaultAsync(so => so.SheetStateOperation.Text == "Saved");

            if (so == null)
            {
                return NotFound();
            }

            ViewData["IsCheck"] = await _context.Evaluations.Where(ev => ev.EvaluationId == input.EvaluationID && ev.Points.Any(p => p.PointId == input.PointID)).AnyAsync();

            return PartialView("/Areas/Formation/Views/Evaluations/Edit/Edit_Table_Point_SO.cshtml", so);
        }










        // GET: Formation/Evaluations/Details/5
        public async Task<IActionResult> Details(long? id)
        {
            if (id == null || _context.Evaluations == null)
            {
                return NotFound();
            }

            var evaluation = await _context
                .Evaluations
                .Include(ev => ev.Employee)
                .Include(ev => ev.Sheet)
                .ThenInclude(sh => sh.Equipment)
                .FirstOrDefaultAsync(ev => ev.EvaluationId == id);

            if (evaluation == null)
            {
                return NotFound();
            }

            ViewData["Categories"] = await _context.SheetOperations
                .Where(so =>
                   so.SheetId == evaluation.SheetId &&
                   so.SheetStateOperation.Text == "Saved" &&
                   so.SheetTypeOperation.Text != "Remove" &&
                   so.DateSaved >= _context.SheetOperations.Where(sod => sod.SheetId == so.SheetId && sod.PointId == so.PointId).Max(sod => sod.DateSaved)
                )
                .Select(so => so.Point.Category)
                .Distinct()
                .ToListAsync();

            return View("/Areas/Formation/Views/Evaluations/Details/Details.cshtml", evaluation);
        }




        public async Task<IActionResult> Details_Table_Category([Bind("CategoryID, EvaluationID")] SheetEvaluationCategoryVM input)
        {
            Category category = await _context.Categories.FindAsync(input.CategoryID);
            Evaluation evaluation = await _context.Evaluations.FindAsync(input.EvaluationID);

            if (category == null || evaluation == null)
            {
                return NotFound();
            }

            ViewData["Points"] = await _context.Points
                .Where(p => p.CategoryId == category.CategoryId && p.SheetOperations.Any(so =>
                        so.SheetId == evaluation.SheetId
                        && so.DateSaved.HasValue
                        && so.SheetTypeOperation.Text != "Remove"
                        && so.DateSaved == _context.SheetOperations
                            .Where(sod => sod.SheetId == so.SheetId
                                && so.PointId == sod.PointId)
                            .Max(sod => sod.DateSaved))
                ).ToListAsync();

            return PartialView("/Areas/Formation/Views/Evaluations/Details/Details_Table_Category.cshtml", category);
        }








        public async Task<IActionResult> Details_Table_Point_SO([Bind("PointID, EvaluationID")] SheetEvaluationPointVM input)
        {
            SheetOperation so = await _context.SheetOperations
                .Where(so => so.Sheet.Evaluations.Any(ev => ev.EvaluationId == input.EvaluationID) && so.PointId == input.PointID)
                .Include(so => so.SheetTypeOperation)
                .Include(so => so.Level)
                .Include(so => so.SheetStateOperation)
                .Include(so => so.Point)
                .OrderByDescending(so => so.DateOperation)
                .FirstOrDefaultAsync(so => so.SheetStateOperation.Text == "Saved");

            if (so == null)
            {
                return NotFound();
            }

            ViewData["IsCheck"] = await _context.Evaluations.Where(ev => ev.EvaluationId == input.EvaluationID && ev.Points.Any(p => p.PointId == input.PointID)).AnyAsync();

            return PartialView("/Areas/Formation/Views/Evaluations/Details/Details_Table_Point_SO.cshtml", so);
        }













        private bool EvaluationExists(long id)
        {
          return (_context.Evaluations?.Any(e => e.EvaluationId == id)).GetValueOrDefault();
        }
    }
}
