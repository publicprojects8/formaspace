﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using Formation.Models.DataBase;

namespace Formation.Areas.Formation.API
{
    [Area("Formation")]
    public class CertificationsController : Controller
    {
        private readonly FormationContext _context;

        public CertificationsController(FormationContext context)
        {
            _context = context;
        }

        // GET: Formation/Certifications
        public async Task<IActionResult> Index()
        {
            var formationContext = _context.Certifications.Include(c => c.Evaluation).Include(c => c.Level);
            return View(await formationContext.ToListAsync());
        }
                
        // GET: Formation/Certifications/Details/5
        public async Task<IActionResult> Details(long? id)
        {
            if (id == null || _context.Certifications == null)
            {
                return NotFound();
            }

            var certification = await _context.Certifications
                .Include(c => c.Evaluation)
                .ThenInclude(c => c.Sheet)
                .ThenInclude(s => s.Equipment)
                .Include(c => c.Level)
                .FirstOrDefaultAsync(m => m.CertificationId == id);
            if (certification == null)
            {
                return NotFound();
            }

            return View(certification);
        }

        // GET: Formation/Certifications/Create
        public async Task<IActionResult> Create(long id)
        {
            Evaluation eval = await _context
                .Evaluations
                .Include(ev => ev.Points)
                .FirstOrDefaultAsync(ev => ev.EvaluationId == id);

            if(eval == null) return NotFound();

            var level = await _context.Levels
                .Where(le => le.SheetOperations
                            .Where(so => so.SheetId == eval.SheetId
                                    && so.DateSaved.HasValue
                                    && so.SheetTypeOperation.Text != "Remove"
                                    && so.DateSaved == _context.SheetOperations
                                        .Where(sod => sod.SheetId == so.SheetId
                                            && so.PointId == sod.PointId)
                                        .Max(sod => sod.DateSaved))
                            .All(so => eval.Points.Contains(so.Point)))
                .OrderByDescending(le => le.Level1)
                .FirstOrDefaultAsync();

            Certification cert = new Certification
            {
                EvaluationId = id,
                Evaluation = eval,
                StartDate = DateTime.Now,
                EndDate = DateTime.Now.AddMonths(6),
                LevelId = level == null ? (await _context.Levels.FirstOrDefaultAsync(le => le.Level1 == _context.Levels.Min(le1 => le1.Level1))).LevelId : level.LevelId,
                Level = level == null ? await _context.Levels.FirstOrDefaultAsync(le => le.Level1 == _context.Levels.Min(le1 => le1.Level1)) : level
            };
            ViewData["EvaluationId"] = id;
            ViewData["LevelId"] = new SelectList(_context.Levels, "LevelId", "LevelName");
            return View(cert);
        }

        // POST: Formation/Certifications/Create
        // To protect from overposting attacks, enable the specific properties you want to bind to.
        // For more details, see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Create([Bind("CertificationId,StartDate,EndDate,LevelId,EvaluationId")] Certification certification)
        {
            if (ModelState.IsValid)
            {
                Employee employee = (await _context.Evaluations.Include(e => e.Employee).FirstOrDefaultAsync(e => e.EvaluationId == certification.EvaluationId)).Employee;

                _context.Add(certification);
                await _context.SaveChangesAsync();
                return RedirectToAction("Details", "Employees", new { area = "HumanResources", id = employee.EmployeeId });
            }
            ViewData["EvaluationId"] = new SelectList(_context.Evaluations, "EvaluationId", "EvaluationId", certification.EvaluationId);
            ViewData["LevelId"] = new SelectList(_context.Levels, "LevelId", "LevelId", certification.LevelId);
            return View(certification);
        }

        // GET: Formation/Certifications/Edit/5
        public async Task<IActionResult> Edit(long? id)
        {
            if (id == null || _context.Certifications == null)
            {
                return NotFound();
            }

            var certification = await _context.Certifications.FindAsync(id);
            if (certification == null)
            {
                return NotFound();
            }
            ViewData["EvaluationId"] = new SelectList(_context.Evaluations, "EvaluationId", "EvaluationId", certification.EvaluationId);
            ViewData["LevelId"] = new SelectList(_context.Levels, "LevelId", "LevelId", certification.LevelId);
            return View(certification);
        }

        // POST: Formation/Certifications/Edit/5
        // To protect from overposting attacks, enable the specific properties you want to bind to.
        // For more details, see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Edit(long id, [Bind("CertificationId,StartDate,EndDate,LevelId,EvaluationId")] Certification certification)
        {
            if (id != certification.CertificationId)
            {
                return NotFound();
            }

            if (ModelState.IsValid)
            {
                try
                {
                    _context.Update(certification);
                    await _context.SaveChangesAsync();
                }
                catch (DbUpdateConcurrencyException)
                {
                    if (!CertificationExists(certification.CertificationId))
                    {
                        return NotFound();
                    }
                    else
                    {
                        throw;
                    }
                }
                return RedirectToAction(nameof(Index));
            }
            ViewData["EvaluationId"] = new SelectList(_context.Evaluations, "EvaluationId", "EvaluationId", certification.EvaluationId);
            ViewData["LevelId"] = new SelectList(_context.Levels, "LevelId", "LevelId", certification.LevelId);
            return View(certification);
        }

        // GET: Formation/Certifications/Delete/5
        public async Task<IActionResult> Delete(long? id)
        {
            if (id == null || _context.Certifications == null)
            {
                return NotFound();
            }

            var certification = await _context.Certifications
                .Include(c => c.Evaluation)
                .Include(c => c.Level)
                .FirstOrDefaultAsync(m => m.CertificationId == id);
            if (certification == null)
            {
                return NotFound();
            }

            return View(certification);
        }

        // POST: Formation/Certifications/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> DeleteConfirmed(long id)
        {
            if (_context.Certifications == null)
            {
                return Problem("Entity set 'FormationContext.Certifications'  is null.");
            }
            var certification = await _context.Certifications.FindAsync(id);
            if (certification != null)
            {
                _context.Certifications.Remove(certification);
            }
            
            await _context.SaveChangesAsync();
            return RedirectToAction(nameof(Index));
        }

        private bool CertificationExists(long id)
        {
          return (_context.Certifications?.Any(e => e.CertificationId == id)).GetValueOrDefault();
        }
    }
}
