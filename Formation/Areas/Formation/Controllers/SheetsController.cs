﻿#nullable disable
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using Formation.Models.DataBase;
using Formation.Areas.Formation.Models;

namespace Formation.Areas.Formation.Controllers
{
    [Area("Formation")]
    public class SheetsController : Controller
    {
        private readonly FormationContext _context;

        public SheetsController(FormationContext context)
        {
            _context = context;
        }








        // GET: Formation/Sheets/Edit/5
        public async Task<IActionResult> Edit(long? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var sheet = await _context.Sheets
                .Include(s => s.Equipment)
                .Include(s => s.SheetOperations
                    .Where(so => so.SheetTypeOperation.Text == ""))
                .ThenInclude(so => so.Point)
                .FirstOrDefaultAsync(m => m.SheetId == id);

            if (sheet == null)
            {
                return NotFound();
            }
            ViewData["Categories"] = await _context.Categories.Where(c => c.Points.Any(p => p.SheetOperations.Any(sh => sh.SheetId == id))).Distinct().ToListAsync();
            ViewData["EquipmentId"] = new SelectList(_context.Equipment, "EquipmentId", "EquipmentId", sheet.EquipmentId);

            ViewData["AllCategories"] = await _context.Categories.ToListAsync();
            ViewData["AllPoints"] = await _context.Points.ToListAsync();

            return View("/Areas/Formation/Views/Sheets/Edit/Edit.cshtml", sheet);
        }

        public async Task<IActionResult> Edit_Table_Category([Bind("CategoryID, SheetID")]EditSheetCategoryVM input)
        {
            Category category = await _context.Categories.FindAsync(input.CategoryID);
            Sheet sheet = await _context.Sheets.FindAsync(input.SheetID);

            if (category == null || sheet == null)
            {
                return NotFound();
            }

            ViewData["Points"] = await _context.Points
                .Where(p => p.CategoryId == category.CategoryId && p.SheetOperations.Any(so =>
                        so.SheetId == sheet.SheetId
                        && !so.DateSaved.HasValue
                        ||
                        (so.SheetTypeOperation.Text != "Remove"
                        && so.DateSaved.HasValue
                        && so.DateSaved == _context.SheetOperations
                            .Where(sod => sod.SheetId == so.SheetId
                                && so.PointId == sod.PointId)
                            .Max(sod => sod.DateSaved)))
                ).ToListAsync();
            return PartialView("/Areas/Formation/Views/Sheets/Edit/Edit_Table_Category.cshtml", category);
        }

        public async Task<IActionResult> Edit_Table_Point_SO([Bind("PointID, SheetID")] EditSheetPointVM input)
        {
            SheetOperation so = await _context.SheetOperations
                .Where(so => so.SheetId == input.SheetID && so.PointId == input.PointID)
                .Include(so => so.SheetTypeOperation)
                .Include(so => so.SheetStateOperation)
                .Include(so => so.Point)
                .OrderByDescending(so => so.DateOperation)
                .FirstOrDefaultAsync();

            if (so == null)
            {
                return NotFound();
            }

            ViewData["Levels"] = new SelectList(_context.Levels, "LevelId", "LevelName", so.LevelId); ;

            return PartialView("/Areas/Formation/Views/Sheets/Edit/Edit_Table_Point_SO.cshtml", so);
        }
















        // GET: Formation/Sheets/Edit/5
        public async Task<IActionResult> Details(long? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var sheet = await _context.Sheets
                .Include(s => s.Equipment)
                .Include(s => s.SheetOperations).ThenInclude(so => so.Point)
                .FirstOrDefaultAsync(m => m.SheetId == id);

            if (sheet == null)
            {
                return NotFound();
            }

            var data = await _context.SheetOperations.Where(so => so.SheetId == id && so.DateSaved.HasValue && so.DateSaved == _context.SheetOperations.Where(sod => sod.SheetId == so.SheetId && so.PointId == sod.PointId).Max(sod => sod.DateSaved)).ToListAsync();

            ViewData["Categories"] = await _context.SheetOperations
                .Where(so => so.SheetId == id
                    && so.SheetTypeOperation.Text != "Remove"
                    && so.DateSaved.HasValue
                    && so.DateSaved == _context.SheetOperations
                        .Where(sod => sod.SheetId == so.SheetId 
                            && so.PointId == sod.PointId)
                        .Max(sod => sod.DateSaved))
                .Select(so => so.Point.Category)
                .Distinct()
                .ToListAsync();

            ViewData["EquipmentId"] = new SelectList(_context.Equipment, "EquipmentId", "EquipmentId", sheet.EquipmentId);

            ViewData["AllCategories"] = await _context.Categories.ToListAsync();
            ViewData["AllPoints"] = await _context.Points.ToListAsync();

            return View("/Areas/Formation/Views/Sheets/Details/Details.cshtml", sheet);
        }

        public async Task<IActionResult> Details_Table_Category([Bind("CategoryID, SheetID")] EditSheetCategoryVM input)
        {
            Category category = await _context.Categories.FindAsync(input.CategoryID);
            Sheet sheet = await _context.Sheets.FindAsync(input.SheetID);

            if (category == null || sheet == null)
            {
                return NotFound();
            }

            ViewData["Points"] = await _context.Points
                .Where(p => p.CategoryId == category.CategoryId 
                    && p.SheetOperations.Any(so => 
                        so.SheetId == sheet.SheetId 
                        && so.SheetTypeOperation.Text != "Remove"
                        && so.DateSaved.HasValue
                        && so.DateSaved == _context.SheetOperations
                            .Where(sod => sod.SheetId == so.SheetId
                                && so.PointId == sod.PointId)
                            .Max(sod => sod.DateSaved)
                    ))
                .ToListAsync();
            return PartialView("/Areas/Formation/Views/Sheets/Details/Details_Table_Category.cshtml", category);
        }

        public async Task<IActionResult> Details_Table_Point_SO([Bind("PointID, SheetID")] EditSheetPointVM input)
        {
            SheetOperation so = await _context.SheetOperations
                .Where(so => so.SheetId == input.SheetID && so.PointId == input.PointID)
                .Include(so => so.SheetTypeOperation)
                .Include(so => so.SheetStateOperation)
                .Include(so => so.Point)
                .OrderByDescending(so => so.DateOperation)
                .FirstOrDefaultAsync();

            if (so == null)
            {
                return NotFound();
            }

            ViewData["Levels"] = new SelectList(_context.Levels, "LevelId", "LevelName", so.LevelId); ;

            return PartialView("/Areas/Formation/Views/Sheets/Details/Details_Table_Point_SO.cshtml", so);
        }


    }
}
