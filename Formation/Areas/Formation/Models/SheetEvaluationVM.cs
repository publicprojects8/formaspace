﻿namespace Formation.Areas.Formation.Models
{
    public class SheetEvaluationCategoryVM
    {
        public long CategoryID { get; set; }

        public long EvaluationID { get; set;}
    }

    public class SheetEvaluationPointVM
    {
        public long PointID { get; set; }

        public long EvaluationID { get; set; }
    }

    public class EvaluationCheckVM
    {
        public long EvaluationID { get; set; }

        public long PointID { get; set; }
    }
}
