﻿namespace Formation.Areas.Formation.Models
{
    public class EditSheetCategoryVM
    {
        public long CategoryID { get; set; }

        public long SheetID { get; set;}
    }

    public class EditSheetPointVM
    {
        public long PointID { get; set; }

        public long SheetID { get; set; }
    }
}
