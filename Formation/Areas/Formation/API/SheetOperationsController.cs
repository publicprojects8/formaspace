﻿#nullable disable
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Formation.Models.DataBase;

namespace Formation.Areas.Formation.API
{
    [Area("Formation")]
    [Route("api/[area]/[controller]")]
    [ApiController]
    public class SheetOperationsController : ControllerBase
    {
        private readonly FormationContext _context;

        public SheetOperationsController(FormationContext context)
        {
            _context = context;
        }


        // GET: api/SheetOperations
        [HttpGet]
        public async Task<ActionResult<IEnumerable<SheetOperation>>> GetSheetOperations()
        {
            return await _context.SheetOperations.ToListAsync();
        }


        // GET: api/SheetOperations/5
        [HttpGet("{id}")]
        public async Task<ActionResult<SheetOperation>> GetSheetOperation(long id)
        {
            var sheetOperation = await _context.SheetOperations.FindAsync(id);

            if (sheetOperation == null)
            {
                return NotFound();
            }

            return sheetOperation;
        }


        // POST: api/SheetOperations
        // To protect from overposting attacks, see https://go.microsoft.com/fwlink/?linkid=2123754
        [HttpPost]
        public async Task<ActionResult<SheetOperation>> PostSheetOperation([Bind("SheetID, PointId, LevelId")]SheetOperation sheetOperation)
        {
            sheetOperation.DateOperation = DateTime.Now;
            sheetOperation.SheetStateOperation = await _context.SheetTypeStates.FirstOrDefaultAsync(sts => sts.Text == "Unsaved");
            sheetOperation.SheetTypeOperation = await _context.SheetTypeOperations.FirstOrDefaultAsync(sto => sto.Text == sheetOperation.SheetTypeOperation.Text);
            sheetOperation.Level = await _context.Levels.FirstOrDefaultAsync(l => l.LevelId == sheetOperation.LevelId);
            _context.SheetOperations.Add(sheetOperation);
            try
            {
                await _context.SaveChangesAsync();
            }
            catch (DbUpdateException)
            {
                if (SheetOperationExists(sheetOperation.SheetOperationId))
                {
                    return Conflict();
                }
                else
                {
                    throw;
                }
            }

            return CreatedAtAction("GetSheetOperation", new { id = sheetOperation.SheetOperationId }, sheetOperation);
        }

        
        // DELETE: api/SheetOperations/5
        [HttpDelete("{id}")]
        public async Task<IActionResult> DeleteSheetOperation(long id)
        {
            var sheetOperation = await _context.SheetOperations.FindAsync(id);
            if (sheetOperation == null)
            {
                return NotFound();
            }

            _context.SheetOperations.Remove(sheetOperation);
            await _context.SaveChangesAsync();

            return NoContent();
        }

        /*
        // PUT: api/SheetOperations/5
        // To protect from overposting attacks, see https://go.microsoft.com/fwlink/?linkid=2123754
        [HttpPut("{id}")]
        public async Task<IActionResult> PutSheetOperation(long id, SheetOperation sheetOperation)
        {
            if (id != sheetOperation.SheetOperationId)
            {
                return BadRequest();
            }

            _context.Entry(sheetOperation).State = EntityState.Modified;

            try
            {
                await _context.SaveChangesAsync();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!SheetOperationExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return NoContent();
        }*/

        private bool SheetOperationExists(long id)
        {
            return _context.SheetOperations.Any(e => e.SheetOperationId == id);
        }
    }
}
