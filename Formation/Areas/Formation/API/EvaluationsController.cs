﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Formation.Models.DataBase;
using Formation.Areas.Formation.Models;

namespace Formation.Areas.Formation.API
{
    [Area("Formation")]
    [Route("api/[area]/[controller]")]
    [ApiController]
    public class EvaluationsController : ControllerBase
    {
        private readonly FormationContext _context;


        // GET: api/Evaluations
        [HttpGet]
        public async Task<ActionResult<IEnumerable<Evaluation>>> GetEvaluations()
        {
            if (_context.Evaluations == null)
            {
                return NotFound();
            }
            return await _context.Evaluations.ToListAsync();
        }

        // GET: api/Evaluations
        /*[HttpGet]
        [Route("Employee")]
        public async Task<ActionResult<IEnumerable<Evaluation>>> GetEvaluations(long id)
        {
            if (_context.Employees == null)
            {
                return NotFound();
            }
            var employee = await _context.Employees
                .FirstOrDefaultAsync(e => e.EmployeeId == id);

            if (employee == null)
            {
                return NotFound();
            }

            var evaluations = await _context.Evaluations
                .Include(e => e.Certification)
                .Include()

            return evaluations;
        }*/

        // GET: api/Evaluations/5
        [HttpGet("{id}")]
        public async Task<ActionResult<Evaluation>> GetEvaluation(long id)
        {
            if (_context.Evaluations == null)
            {
                return NotFound();
            }
            var evaluation = await _context.Evaluations.FindAsync(id);

            if (evaluation == null)
            {
                return NotFound();
            }

            return evaluation;
        }

        public EvaluationsController(FormationContext context)
        {
            _context = context;
        }

        [HttpPost]
        [Route("Check")]
        public async Task<ActionResult<Evaluation>> Check([Bind("EvaluationID,PointID")] EvaluationCheckVM input)
        {
            Evaluation evaluation = await _context.Evaluations.FindAsync(input.EvaluationID);
            Point point = await _context.Points.FindAsync(input.PointID);

            if (evaluation == null || point == null) return NotFound();

            evaluation.Points.Add(point);

            await _context.SaveChangesAsync();

            return evaluation;
        }

        [HttpPost]
        [Route("UnCheck")]
        public async Task<ActionResult<Evaluation>> Uncheck([Bind("EvaluationID,PointID")] EvaluationCheckVM input)
        {
            Evaluation evaluation = await _context.Evaluations
                .Include(e => e.Points)
                .FirstOrDefaultAsync(ev => ev.EvaluationId == input.EvaluationID);
            Point point = await _context.Points.FindAsync(input.PointID);

            if (evaluation == null || point == null) return NotFound();

            evaluation.Points.Remove(point);

            await _context.SaveChangesAsync();

            return evaluation;
        }

        [HttpPost]
        [Route("Close/{id}")]
        public async Task<ActionResult<Evaluation>> Close(long id)
        {
            Evaluation evaluation = await _context.Evaluations.FindAsync(id);

            if(evaluation == null)
            {
                return NotFound();
            }

            evaluation.DateEndEval = DateTime.Now;

            await _context.SaveChangesAsync();

            return evaluation;
        }






        // PUT: api/Evaluations/5
        // To protect from overposting attacks, see https://go.microsoft.com/fwlink/?linkid=2123754
        /*[HttpPut("{id}")]
        public async Task<IActionResult> PutEvaluation(long id, Evaluation evaluation)
        {
            if (id != evaluation.EvaluationId)
            {
                return BadRequest();
            }

            _context.Entry(evaluation).State = EntityState.Modified;

            try
            {
                await _context.SaveChangesAsync();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!EvaluationExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return NoContent();
        }*/

        // POST: api/Evaluations
        // To protect from overposting attacks, see https://go.microsoft.com/fwlink/?linkid=2123754
        /*[HttpPost]
        public async Task<ActionResult<Evaluation>> PostEvaluation(Evaluation evaluation)
        {
          if (_context.Evaluations == null)
          {
              return Problem("Entity set 'FormationContext.Evaluations'  is null.");
          }
            _context.Evaluations.Add(evaluation);
            await _context.SaveChangesAsync();

            return CreatedAtAction("GetEvaluation", new { id = evaluation.EvaluationId }, evaluation);
        }*/

        // DELETE: api/Evaluations/5
        /*[HttpDelete("{id}")]
        public async Task<IActionResult> DeleteEvaluation(long id)
        {
            if (_context.Evaluations == null)
            {
                return NotFound();
            }
            var evaluation = await _context.Evaluations.FindAsync(id);
            if (evaluation == null)
            {
                return NotFound();
            }

            _context.Evaluations.Remove(evaluation);
            await _context.SaveChangesAsync();

            return NoContent();
        }*/

        /*private bool EvaluationExists(long id)
        {
            return (_context.Evaluations?.Any(e => e.EvaluationId == id)).GetValueOrDefault();
        }*/
    }
}
