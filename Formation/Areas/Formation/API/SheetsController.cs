﻿#nullable disable
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Formation.Models.DataBase;

namespace Formation.Areas.Formation.API
{
    [Area("Formation")]
    [Route("api/[area]/[controller]")]
    [ApiController]
    public class SheetsController : ControllerBase
    {
        private readonly FormationContext _context;

        public SheetsController(FormationContext context)
        {
            _context = context;
        }

        // GET: api/Sheets
        [HttpGet]
        public async Task<ActionResult<IEnumerable<Sheet>>> GetSheets()
        {
            return await _context.Sheets.ToListAsync();
        }

        // GET: api/Sheets/5
        [HttpGet("{id}")]
        public async Task<ActionResult<Sheet>> GetSheet(long id)
        {
            var sheet = await _context.Sheets.FindAsync(id);

            if (sheet == null)
            {
                return NotFound();
            }

            return sheet;
        }

        [Route("Save/{id}")]
        [HttpPost()]
        public async Task<ActionResult<Sheet>> Update(long id)
        {
            var sheetUnsaved = await _context.Sheets.FindAsync(id);

            if (sheetUnsaved == null)
            {
                return NotFound();
            }

            var stateSaved = await _context.SheetTypeStates.FirstAsync(sts => sts.Text == "Saved");
            var stateUnsaved = await _context.SheetTypeStates.FirstAsync(sts => sts.Text == "Unsaved");

            foreach (var so in await _context.SheetOperations.Where(so => so.SheetId == id && so.SheetStateOperationId == stateUnsaved.SheetStateOperationId).ToListAsync())
            {
                so.SheetStateOperation = stateSaved;
                so.DateSaved = DateTime.Now;
            }

            await _context.SaveChangesAsync();

            return CreatedAtAction("GetSheet", new { id = id }, id);
        }

        // PUT: api/Sheets/5
        // To protect from overposting attacks, see https://go.microsoft.com/fwlink/?linkid=2123754
        /*[HttpPut("{id}")]
        public async Task<IActionResult> PutSheet(long id, Sheet sheet)
        {
            if (id != sheet.SheetId)
            {
                return BadRequest();
            }

            _context.Entry(sheet).State = EntityState.Modified;

            try
            {
                await _context.SaveChangesAsync();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!SheetExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return NoContent();
        }*/

        // POST: api/Sheets
        // To protect from overposting attacks, see https://go.microsoft.com/fwlink/?linkid=2123754
        /*[HttpPost]
        public async Task<ActionResult<Sheet>> PostSheet(Sheet sheet)
        {
            _context.Sheets.Add(sheet);
            await _context.SaveChangesAsync();

            return CreatedAtAction("GetSheet", new { id = sheet.SheetId }, sheet);
        }*/

        // DELETE: api/Sheets/5
        /*[HttpDelete("{id}")]
        public async Task<IActionResult> DeleteSheet(long id)
        {
            var sheet = await _context.Sheets.FindAsync(id);
            if (sheet == null)
            {
                return NotFound();
            }

            _context.Sheets.Remove(sheet);
            await _context.SaveChangesAsync();

            return NoContent();
        }*/

        private bool SheetExists(long id)
        {
            return _context.Sheets.Any(e => e.SheetId == id);
        }
    }
}
