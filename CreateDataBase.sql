CREATE TABLE Equipment(
   EquipmentID BIGINT IDENTITY,
   Name VARCHAR(255) NOT NULL,
   PRIMARY KEY(EquipmentID),
   UNIQUE(Name)
);

CREATE TABLE Employee(
   EmployeeID BIGINT IDENTITY,
   FirstName VARCHAR(255) NOT NULL,
   LastName VARCHAR(255) NOT NULL,
   PRIMARY KEY(EmployeeID)
);

CREATE TABLE Sheets(
   SheetID BIGINT IDENTITY,
   EquipmentID BIGINT,
   PRIMARY KEY(SheetID),
   FOREIGN KEY(EquipmentID) REFERENCES Equipment(EquipmentID)
);

CREATE TABLE Category(
   CategoryID BIGINT IDENTITY,
   Name VARCHAR(255) NOT NULL,
   PRIMARY KEY(CategoryID),
   UNIQUE(Name)
);

CREATE TABLE Evaluation(
   EvaluationID BIGINT IDENTITY,
   DateStartEval DATETIME NOT NULL,
   DateEndEval DATETIME,
   ObserverComment VARCHAR(255),
   EmployeeID BIGINT,
   SheetID BIGINT,
   EmployeeID_1 BIGINT,
   PRIMARY KEY(EvaluationID),
   FOREIGN KEY(EmployeeID) REFERENCES Employee(EmployeeID),
   FOREIGN KEY(SheetID) REFERENCES Sheets(SheetID),
   FOREIGN KEY(EmployeeID_1) REFERENCES Employee(EmployeeID)
);

CREATE TABLE Level(
   LevelID BIGINT IDENTITY,
   Level BIGINT NOT NULL,
   LevelColor VARCHAR(255),
   LevelName VARCHAR(255) NOT NULL,
   PRIMARY KEY(LevelID),
   UNIQUE(Level),
   UNIQUE(LevelName)
);

CREATE TABLE SheetTypeOperation(
   SheetTypeOperationID BIGINT IDENTITY,
   Text VARCHAR(255) NOT NULL,
   PRIMARY KEY(SheetTypeOperationID),
   UNIQUE(Text)
);

CREATE TABLE SheetTypeState(
   SheetStateOperationID BIGINT IDENTITY,
   Text VARCHAR(255) NOT NULL,
   PRIMARY KEY(SheetStateOperationID),
   UNIQUE(Text)
);

CREATE TABLE Points(
   PointID BIGINT IDENTITY,
   Text VARCHAR(255),
   CategoryID BIGINT,
   PRIMARY KEY(PointID),
   FOREIGN KEY(CategoryID) REFERENCES Category(CategoryID)
);

CREATE TABLE Certification(
   CertificationID BIGINT IDENTITY,
   StartDate DATETIME NOT NULL,
   EndDate DATETIME NOT NULL,
   LevelID BIGINT,
   EvaluationID BIGINT,
   PRIMARY KEY(CertificationID),
   UNIQUE(EvaluationID),
   FOREIGN KEY(LevelID) REFERENCES Level(LevelID),
   FOREIGN KEY(EvaluationID) REFERENCES Evaluation(EvaluationID)
);

CREATE TABLE SheetOperation(
   SheetOperationID BIGINT IDENTITY,
   DateOperation DATETIME NOT NULL,
   DateSaved DATETIME,
   SheetID BIGINT,
   LevelID BIGINT,
   PointID BIGINT,
   SheetTypeOperationID BIGINT,
   SheetStateOperationID BIGINT,
   PRIMARY KEY(SheetOperationID),
   FOREIGN KEY(SheetID) REFERENCES Sheets(SheetID),
   FOREIGN KEY(LevelID) REFERENCES Level(LevelID),
   FOREIGN KEY(PointID) REFERENCES Points(PointID),
   FOREIGN KEY(SheetTypeOperationID) REFERENCES SheetTypeOperation(SheetTypeOperationID),
   FOREIGN KEY(SheetStateOperationID) REFERENCES SheetTypeState(SheetStateOperationID)
);

CREATE TABLE Ev_Po(
   PointID BIGINT,
   EvaluationID BIGINT,
   PRIMARY KEY(PointID, EvaluationID),
   FOREIGN KEY(PointID) REFERENCES Points(PointID),
   FOREIGN KEY(EvaluationID) REFERENCES Evaluation(EvaluationID)
);
